CREATE DATABASE  IF NOT EXISTS `itt_base` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `itt_base`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: base10    Database: itt_base
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item` (
  `iditem` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Справочник изделий',
  `itemName` varchar(45) NOT NULL,
  `itemNameDec` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`iditem`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `param`
--

DROP TABLE IF EXISTS `param`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param` (
  `idparam` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `paramName` varchar(45) NOT NULL,
  `paramNote` mediumtext,
  PRIMARY KEY (`idparam`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `proc`
--

DROP TABLE IF EXISTS `proc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proc` (
  `idproc` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `procidtest` int(10) unsigned NOT NULL,
  `procidparam` int(10) unsigned NOT NULL,
  `procDateTime` datetime NOT NULL,
  `procvalue` double DEFAULT NULL,
  `procnote` varchar(255) DEFAULT NULL,
  `procbinary` longblob,
  `procMime` longblob,
  `procMimeType` varchar(45) DEFAULT NULL,
  `mimesize` int(11) GENERATED ALWAYS AS (length(`procMime`)) STORED,
  PRIMARY KEY (`idproc`),
  KEY `FK_ITEM_idx` (`procidparam`),
  KEY `FK_TEST_idx` (`procidtest`),
  KEY `cnt_proc` (`procidtest`,`procidparam`),
  CONSTRAINT `FK_PARAM` FOREIGN KEY (`procidparam`) REFERENCES `param` (`idparam`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TEST` FOREIGN KEY (`procidtest`) REFERENCES `test` (`idtest`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=107297 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `system`
--

DROP TABLE IF EXISTS `system`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system` (
  `id` int(11) NOT NULL,
  `version` bigint(20) NOT NULL,
  `mimeUploadDir` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `idtest` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `testiditem` int(10) unsigned NOT NULL,
  `testItemNumber` varchar(45) DEFAULT NULL,
  `testidworkspace` int(10) unsigned NOT NULL,
  `testNote` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idtest`),
  KEY `FK_WORKSPACE_idx` (`testidworkspace`),
  KEY `FK_ITEM` (`testiditem`),
  KEY `searchable` (`idtest`,`testiditem`,`testidworkspace`),
  CONSTRAINT `FK_ITEM` FOREIGN KEY (`testiditem`) REFERENCES `item` (`iditem`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_WORKSPACE` FOREIGN KEY (`testidworkspace`) REFERENCES `workspace` (`idworkspace`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=78251 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `iduser` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(60) NOT NULL,
  `userpass` varchar(60) NOT NULL,
  `useradmin` int(1) DEFAULT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `viewproc`
--

DROP TABLE IF EXISTS `viewproc`;
/*!50001 DROP VIEW IF EXISTS `viewproc`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `viewproc` AS SELECT 
 1 AS `idproc`,
 1 AS `procidtest`,
 1 AS `procidparam`,
 1 AS `procDateTime`,
 1 AS `procvalue`,
 1 AS `procnote`,
 1 AS `procbinary`,
 1 AS `procMime`,
 1 AS `procMimeType`,
 1 AS `idtest`,
 1 AS `testiditem`,
 1 AS `testItemNumber`,
 1 AS `testidworkspace`,
 1 AS `testNote`,
 1 AS `idparam`,
 1 AS `paramName`,
 1 AS `paramNote`,
 1 AS `iditem`,
 1 AS `itemName`,
 1 AS `itemNameDec`,
 1 AS `idworkspace`,
 1 AS `workspaceName`,
 1 AS `workspaceNumber`,
 1 AS `workspaceMisc`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `workspace`
--

DROP TABLE IF EXISTS `workspace`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workspace` (
  `idworkspace` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `workspaceName` varchar(45) NOT NULL,
  `workspaceNumber` varchar(45) NOT NULL,
  `workspaceMisc` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idworkspace`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COMMENT='Справочник рабочих мест';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Final view structure for view `viewproc`
--

/*!50001 DROP VIEW IF EXISTS `viewproc`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`user`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `viewproc` AS (select `proc`.`idproc` AS `idproc`,`proc`.`procidtest` AS `procidtest`,`proc`.`procidparam` AS `procidparam`,`proc`.`procDateTime` AS `procDateTime`,`proc`.`procvalue` AS `procvalue`,`proc`.`procnote` AS `procnote`,`proc`.`procbinary` AS `procbinary`,`proc`.`procMime` AS `procMime`,`proc`.`procMimeType` AS `procMimeType`,`test`.`idtest` AS `idtest`,`test`.`testiditem` AS `testiditem`,`test`.`testItemNumber` AS `testItemNumber`,`test`.`testidworkspace` AS `testidworkspace`,`test`.`testNote` AS `testNote`,`param`.`idparam` AS `idparam`,`param`.`paramName` AS `paramName`,`param`.`paramNote` AS `paramNote`,`item`.`iditem` AS `iditem`,`item`.`itemName` AS `itemName`,`item`.`itemNameDec` AS `itemNameDec`,`workspace`.`idworkspace` AS `idworkspace`,`workspace`.`workspaceName` AS `workspaceName`,`workspace`.`workspaceNumber` AS `workspaceNumber`,`workspace`.`workspaceMisc` AS `workspaceMisc` from ((((`proc` join `test`) join `param`) join `item`) join `workspace`) where ((`proc`.`procidtest` = `test`.`idtest`) and (`proc`.`procidparam` = `param`.`idparam`) and (`test`.`testiditem` = `item`.`iditem`) and (`test`.`testidworkspace` = `workspace`.`idworkspace`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-07 16:26:55
