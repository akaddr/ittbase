<?php

class ProcController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
        return array(
            array('allow',
                'actions'=>array('index','view','itemlist','itemlistselected','procmime','ParamsByTestAndItem'),
                'users'=>array('@'),
            ),
            array('allow',
                'actions'=>array('admin','delete','create','update'),
                'users'=>$this->getAdminUsersArray(),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Proc;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Proc']))
		{
			$model->attributes=$_POST['Proc'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->idproc));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Proc']))
		{
			$model->attributes=$_POST['Proc'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->idproc));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

    public function actionIndex()
    {
        $model=new Proc('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Proc']))
            $model->attributes=$_GET['Proc'];

        //custom filters
        if (isset($_GET['items']) && $_GET['items']>0) {
            $model->attributes = array_merge($model->attributes,array('testiditem'=>$_GET['items']));
        }
        if (isset($_GET['tests']) && $_GET['tests']>0) {
            $model->attributes = array_merge($model->attributes,array('procidtest'=>$_GET['tests']));
        }
        if (isset($_GET['params']) && $_GET['params']>0) {
            $model->attributes = array_merge($model->attributes,array('procidparam'=>$_GET['params']));
        }
        if (isset($_GET['workspace']) && $_GET['workspace']>0) {
            $model->attributes = array_merge($model->attributes,array('workspaceid_search'=>$_GET['workspace']));
        }

        $this->render('index',array(
            'model'=>$model,
        ));
    }

    public function actionProcMime($id)
    {
        $procmime = $this->loadModel($id);
        if (strlen($procmime->procMime)>0) {
            $this->renderPartial('procmime',array(
                'procmime'=>$procmime,
            ));
        } else {
            throw new CHttpException(404,'Страница не найдена.');
        }
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Proc the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Proc::model()->findByPk($id);
		if($model===null)
            throw new CHttpException(404,'Страница не найдена.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Proc $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='proc-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

/*    public function actionParamsByTestAndItem() {
        $test = Yii::app()->request->getPost('test',0);

        $crit = new CDbCriteria;
        $crit->distinct = true;
        $crit->select = "procidparam";
        $crit->condition = 'procidtest='.$test;

        $result = Proc::model()->findAll($crit);
        $options = '';
        if (count($result)>0) {
            $options = CHtml::tag('option', array('value'=>''),CHtml::encode('Выберите параметр'),true);
            foreach($result as $proc) {
                $param = Param::model()->findByPk($proc['procidparam']);
                $options .= CHtml::tag('option', array('value'=>$param['idparam']),CHtml::encode($param['paramName']),true);
            }
        }
        echo json_encode(array('result'=>$options));
    }*/

}
