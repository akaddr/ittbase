<?php
/* @var $this ProcController */
/* @var $model Proc */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'proc-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>

    <p class="note">Поля со звёздочкой <span class="required">*</span> обязательны для заполнения.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'procidtest'); ?>
		<?php echo $form->textField($model,'procidtest',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'procidtest'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'procidparam'); ?>
		<?php echo $form->textField($model,'procidparam',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'procidparam'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'procDateTime'); ?>
		<?php echo $form->textField($model,'procDateTime'); ?>
		<?php echo $form->error($model,'procDateTime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'procvalue'); ?>
		<?php echo $form->textField($model,'procvalue'); ?>
		<?php echo $form->error($model,'procvalue'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'procnote'); ?>
		<?php echo $form->textField($model,'procnote',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'procnote'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'procbinary'); ?>
		<?php echo $form->textField($model,'procbinary'); ?>
		<?php echo $form->error($model,'procbinary'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'procMime'); ?>
		<?php echo $form->textField($model,'procMime'); ?>
		<?php echo $form->error($model,'procMime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'procMimeType'); ?>
		<?php echo $form->textField($model,'procMimeType',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'procMimeType'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->