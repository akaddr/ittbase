<?php
/* @var $this ProcController */
/* @var $model Proc */

$this->breadcrumbs=array(
	'Измерения'=>array('index'),
	'Добавление',
);

$this->menu=array(
	array('label'=>'Список измерений', 'url'=>array('index')),
	array('label'=>'Управление измерениями', 'url'=>array('admin')),
);
?>

<h1>Добавление измерения</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>