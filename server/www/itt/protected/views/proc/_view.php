<?php
/* @var $this ProcController */
/* @var $data Proc */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idproc')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idproc), array('view', 'id'=>$data->idproc)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('procidtest')); ?>:</b>
	<?php echo CHtml::encode($data->procidtest); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('procidparam')); ?>:</b>
	<?php echo CHtml::encode($data->procidparam); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('procDateTime')); ?>:</b>
	<?php echo CHtml::encode($data->procDateTime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('procvalue')); ?>:</b>
	<?php echo CHtml::encode($data->procvalue); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('procnote')); ?>:</b>
	<?php echo CHtml::encode($data->procnote); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('procMimeHtml')); ?>:</b>
	<?php echo CHtml::encode($data->procMimeHtml()); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('procMime')); ?>:</b>
	<?php echo CHtml::encode($data->procMime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('procMimeType')); ?>:</b>
	<?php echo CHtml::encode($data->procMimeType); ?>
	<br />

	*/ ?>

</div>