<?php
/* @var $this ProcController */
/* @var $model Proc */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idproc'); ?>
		<?php echo $form->textField($model,'idproc',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'procidtest'); ?>
		<?php echo $form->textField($model,'procidtest',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'procidparam'); ?>
		<?php echo $form->textField($model,'procidparam',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'procDateTime'); ?>
		<?php echo $form->textField($model,'procDateTime'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'procvalue'); ?>
		<?php echo $form->textField($model,'procvalue'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'procnote'); ?>
		<?php echo $form->textField($model,'procnote',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'procbinary'); ?>
		<?php echo $form->textField($model,'procbinary'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'procMime'); ?>
		<?php echo $form->textField($model,'procMime'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'procMimeType'); ?>
		<?php echo $form->textField($model,'procMimeType',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row buttons">
        <?php echo CHtml::submitButton('Найти'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->