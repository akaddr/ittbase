<?php
/* @var $this ProcController */
/* @var $model Proc */

$this->breadcrumbs=array(
	'Измерения'=>array('index'),
	$model->idproc=>array('view','id'=>$model->idproc),
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Список измерений', 'url'=>array('index')),
	array('label'=>'Добавление измерения', 'url'=>array('create')),
	array('label'=>'Просмотр измерений', 'url'=>array('view', 'id'=>$model->idproc))
);
?>

<h1>Редактирование измерения <?php echo $model->idproc; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>