<?php
/* @var $this ProcController */
/* @var $model Proc */

$this->breadcrumbs=array(
	'Procs'=>array('index'),
	$model->idproc,
);

$this->menu=array(
	array('label'=>'Список измерений', 'url'=>array('index')),
	array('label'=>'Добавление измерения', 'url'=>array('create')),
	array('label'=>'Редактировать измерение', 'url'=>array('update', 'id'=>$model->idproc)),
	array('label'=>'Удалить измерение', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idproc),'confirm'=>'Вы точно хотите удалить это измерение?'))
);
?>

<h1>Просмотр измерения №<?php echo $model->idproc; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idproc',
		'procidtest',
		'procidparam',
		'procDateTime',
		'procvalue',
		'procnote',
		'procMimeHtml'=>array('type'=>'html', 'name'=>'procMimeHtml'),
	),
)); ?>
