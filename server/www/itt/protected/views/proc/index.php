<?php
/* @var $this ProcController */
/* @var $model Proc */

$model->dbCriteria->order='idproc DESC';

$this->breadcrumbs=array(
    'Измерения'=>array('index'),
    'Список измерений',
);

/*$this->menu=array(
    array('label'=>'List Proc', 'url'=>array('index')),
    array('label'=>'Create Proc', 'url'=>array('create')),
);*/
?>
<div style="float:right;">
    <a href="#visual_graph" class="showgraph">Построить график</a>
</div>

<?=CHtml::beginForm(array('proc/index'),'get',array('id'=>'filterform'))?>
<?=CHtml::dropDownList('items',Yii::app()->request->getQuery('items',0), Item::allItems(),array(
    'empty'=>'Выберите изделие',
    'onchange'=>'$("#workspace").val(0);$("#tests").val(0);$("#params").val(0);this.form.submit()',
));?>

<?=CHtml::dropDownList(
	'workspace',
	Yii::app()->request->getQuery('workspace',0),
	Yii::app()->request->getQuery('items',0)>0 ? Test::workspacesOfItem(Yii::app()->request->getQuery('items',0)): array(),
	array(
    'style'=> Yii::app()->request->getQuery('items',0)>0 ? '' : 'display:none',
    'empty'=>'Рабочее место',
    'onchange'=>'$("#tests").val(0);this.form.submit()',
));?>

<?
/*CHtml::dropDownList(
    'tests',
    Yii::app()->request->getQuery('tests',0),
    Yii::app()->request->getQuery('workspace',0)>0 ? Test::testsOfItem(Yii::app()->request->getQuery('items',0),Yii::app()->request->getQuery('workspace',0)): array(),
    array(
    'style'=> Yii::app()->request->getQuery('workspace',0)>0 ? '' : 'display:none',
    'empty'=>'Выберите испытание',
    'onchange'=>'$("#params").val(0);this.form.submit()',
));
*/
?>

<?=CHtml::dropDownList(
    'params',
    Yii::app()->request->getQuery('params',0),
    Yii::app()->request->getQuery('workspace',0)>0 ? Proc::paramsOfWorspace(Yii::app()->request->getQuery('workspace',0)) : array(),
    array(
    'style'=> Yii::app()->request->getQuery('workspace',0)>0 ? '' : 'display:none',
    'empty'=>'Выберите параметр',
    'onchange'=>'this.form.submit()',
));?>
<?=CHtml::submitButton('Применить')?>
<?=CHtml::endForm()?>

<?$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'proc-grid',
    'dataProvider'=>$model->search(),
    'cssFile' => Yii::app()->baseUrl . '/css/gridview/gridview.css',
    'filter'=>$model,
    'columns'=>array(
        'idproc',
        array( 'name'=>'workspace_search', 'value'=>'$data->test->workspace->workspaceName'),
        array( 'name'=>'item_search', 'value'=>'$data->test->item->itemName'),
        array( 'name'=>'itemnamedec_search', 'value'=>'$data->test->item->itemNameDec'),
        array( 'name'=>'itemnumber_search', 'value'=>'$data->test->testItemNumber'),
        array( 'name'=>'testNote_search', 'value'=>'$data->test->testNote'),
        array( 'name'=>'param_search', 'value'=>'$data->param->paramName'),
        array('name'=>'procDateTime', 'value'=>'$data->procDateTime','htmlOptions'=>array('class'=>'indexes')),
        array('name'=>'procvalue', 'value'=>'$data->procvalue','htmlOptions'=>array('class'=>'values')),
        'procnote',
        'procMimeHtml'=>array('type'=>'raw', 'name'=>'procMimeHtml'),
        array(
            'class'=>'CButtonColumn',
        ),
    ),
)); ?>

<!--dygraph visualisation for currently displayed data-->
<div id="visual_graph" style="margin: 0 auto; height: 600px; width: 1100px; display:none;"></div>
<script src="js/dygraph-combined.js" type="text/javascript"></script>
<script>
    $(function(){
        var dygraph;
        $('.showgraph').on('click',function(){
            $('#visual_graph').show();
            dygraph = new Dygraph(
                document.getElementById('visual_graph'),
                getCsvData()
            );
            setInterval(function(){
                dygraph.updateOptions({
                    file: getCsvData()
                });
            },1000);
            return false;
        })

        function getCsvData() {
            csvdata = '';
            i=0;
            $('.items tbody tr').each(function(){
                csvdata += i+','+parseFloat($(this).find('td.values').html()) + "\n";
                i++;
            });
            return csvdata;
        }

    })
</script>
<!--end of dygraph-->
<?
$this->widget('application.extensions.fancybox.EFancyBox', array(
        'target' => 'a.mime',
        'config' => array('type'=>'image'),
    )
);

$this->widget('application.extensions.fancybox.EFancyBox', array(
        'target' => 'a.showgraph',
        'config' => array(
            'type'=>'inline',
            'width'=>1000,
            'height'=>600,
            'onClosed'=>'js:function(){$("#visual_graph").hide()}',
        )
    )
);

?>
