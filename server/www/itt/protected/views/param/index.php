<?php
/* @var $this ParamController */
/* @var $model Param */

$this->breadcrumbs=array(
    'Параметры'=>array('index'),
    'Справочник параметров',
);

$this->menu=array(
    array('label'=>'Список', 'url'=>array('index')),
    array('label'=>'Добавить', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#param-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php echo CHtml::link('Расширенный поиск','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search',array(
        'model'=>$model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'param-grid',
    'dataProvider'=>$model->search(),
    'cssFile' => Yii::app()->baseUrl . '/css/gridview/gridview.css',
    'filter'=>$model,
    'columns'=>array(
        'idparam',
        'paramName',
        'paramNote',
        array(
            'class'=>'CButtonColumn',
        ),
    ),
)); ?>
