<?php
/* @var $this ParamController */
/* @var $data Param */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idparam')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idparam), array('view', 'id'=>$data->idparam)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paramName')); ?>:</b>
	<?php echo CHtml::encode($data->paramName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paramNote')); ?>:</b>
	<?php echo CHtml::encode($data->paramNote); ?>
	<br />


</div>