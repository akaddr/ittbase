<?php
/* @var $this ParamController */
/* @var $model Param */

$this->breadcrumbs=array(
	'Параметры'=>array('index'),
	$model->idparam,
);

$this->menu=array(
	array('label'=>'Список', 'url'=>array('index')),
	array('label'=>'Добавить', 'url'=>array('create')),
	array('label'=>'Редактировать', 'url'=>array('update', 'id'=>$model->idparam)),
	array('label'=>'Удалить', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idparam),'confirm'=>'Вы действительно хотите удалить параметр?')),
);
?>

<h1>Просмотр параметра <?php echo $model->idparam; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idparam',
		'paramName',
		'paramNote',
	),
)); ?>
