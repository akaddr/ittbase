<?php
/* @var $this ParamController */
/* @var $model Param */

$this->breadcrumbs=array(
	'Параметры'=>array('index'),
	'Добавление',
);

$this->menu=array(
	array('label'=>'Список', 'url'=>array('index')),
);
?>

<h1>Добавление параметра</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>