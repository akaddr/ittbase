<?php
/* @var $this ParamController */
/* @var $model Param */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'param-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Поля со звёздочкой <span class="required">*</span> обязательны для заполнения.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'paramName'); ?>
		<?php echo $form->textField($model,'paramName',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'paramName'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'paramNote'); ?>
		<?php echo $form->textField($model,'paramNote',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'paramNote'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->