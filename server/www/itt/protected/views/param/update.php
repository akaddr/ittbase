<?php
/* @var $this ParamController */
/* @var $model Param */

$this->breadcrumbs=array(
	'Параметры'=>array('index'),
	$model->idparam=>array('view','id'=>$model->idparam),
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Список', 'url'=>array('index')),
	array('label'=>'Добавить', 'url'=>array('create')),
	array('label'=>'Просмотр', 'url'=>array('view', 'id'=>$model->idparam)),
);
?>

<h1>Редактирование параметра <?php echo $model->idparam; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>