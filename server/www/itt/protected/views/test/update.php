<?php
/* @var $this TestController */
/* @var $model Test */

$this->breadcrumbs=array(
	'Испытания'=>array('index'),
	$model->idtest=>array('view','id'=>$model->idtest),
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Список испытаний', 'url'=>array('index')),
	array('label'=>'Добавить испытание', 'url'=>array('create')),
	array('label'=>'Просмотр', 'url'=>array('view', 'id'=>$model->idtest)),
);
?>

<h1>Редактирование испытания <?php echo $model->idtest; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>