<?php
/* @var $this TestController */
/* @var $model Test */

$this->breadcrumbs=array(
	'Испытания'=>array('index'),
	'Добавление',
);

$this->menu=array(
	array('label'=>'Список испытаний', 'url'=>array('index'))
);
?>

<h1>Создание испытания</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>