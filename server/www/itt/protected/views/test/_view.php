<?php
/* @var $this TestController */
/* @var $data Test */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idtest')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idtest), array('view', 'id'=>$data->idtest)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('testiditem')); ?>:</b>
	<?php echo CHtml::encode($data->testiditem); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('testItemNumber')); ?>:</b>
	<?php echo CHtml::encode($data->testItemNumber); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('testidworkspace')); ?>:</b>
	<?php echo CHtml::encode($data->testidworkspace); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('testNote')); ?>:</b>
	<?php echo CHtml::encode($data->testNote); ?>
	<br />

</div>