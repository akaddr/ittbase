<?php
/* @var $this TestController */
/* @var $model Test */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idtest'); ?>
		<?php echo $form->textField($model,'idtest',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'testiditem'); ?>
		<?php echo $form->textField($model,'testiditem',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'testItemNumber'); ?>
		<?php echo $form->textField($model,'testItemNumber',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'testidworkspace'); ?>
		<?php echo $form->textField($model,'testidworkspace',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'testNote'); ?>
		<?php echo $form->textField($model,'testNote',array('size'=>45,'maxlength'=>45)); ?>
	</div>
	<div class="row buttons">
        <?php echo CHtml::submitButton('Найти'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->