<?php
/* @var $this TestController */
/* @var $model Test */

$this->breadcrumbs=array(
	'Испытания'=>array('index'),
	$model->idtest,
);

$this->menu=array(
	array('label'=>'Список испытаний', 'url'=>array('index')),
	array('label'=>'Добавить испытание', 'url'=>array('create')),
	array('label'=>'Редактировать испытание', 'url'=>array('update', 'id'=>$model->idtest)),
	array('label'=>'Удалить испытание', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idtest),'confirm'=>'Вы действительно хотите удалить запись?')),
);
?>

<h1>Просмотр испытания #<?php echo $model->idtest; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idtest',
		'testiditem',
		'testItemNumber',
		'testidworkspace',
        'testNote',
	),
)); ?>
