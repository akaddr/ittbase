<?php
/* @var $this TestController */
/* @var $model Test */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'test-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Поля со звёздочкой <span class="required">*</span> обязательны для заполнения.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'testiditem'); ?>
		<?php echo $form->textField($model,'testiditem',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'testiditem'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'testItemNumber'); ?>
		<?php echo $form->textField($model,'testItemNumber',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'testItemNumber'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'testidworkspace'); ?>
		<?php echo $form->textField($model,'testidworkspace',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'testidworkspace'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'testNote'); ?>
		<?php echo $form->textField($model,'testNote',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'testNote'); ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->