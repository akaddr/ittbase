<?php
/* @var $this TestController */
/* @var $model Test */

$this->breadcrumbs=array(
    'Испытания'=>array('index'),
    'Просмотр',
);

$this->menu=array(
    array('label'=>'Список испытаний', 'url'=>array('index')),
    array('label'=>'Добавить испытание', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#test-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php echo CHtml::link('Расширенный поиск','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search',array(
        'model'=>$model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'test-grid',
    'dataProvider'=>$model->search(),
    'cssFile' => Yii::app()->baseUrl . '/css/gridview/gridview.css',
    'filter'=>$model,
    'columns'=>array(
        'idtest',
        array( 'name'=>'item_search', 'value'=>'$data->item->itemName'),
        'testItemNumber',
        array( 'name'=>'workspace_search', 'value'=>'$data->workspace->workspaceName'),
        array(
            'class'=>'CButtonColumn',
        ),
    ),
)); ?>
