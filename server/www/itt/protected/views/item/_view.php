<?php
/* @var $this ItemController */
/* @var $data Item */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('iditem')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->iditem), array('view', 'id'=>$data->iditem)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('itemName')); ?>:</b>
	<?php echo CHtml::encode($data->itemName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('itemNameDec')); ?>:</b>
	<?php echo CHtml::encode($data->itemNameDec); ?>
	<br />


</div>