<?php
/* @var $this ItemController */
/* @var $model Item */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'item-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Поля со звёздочкой <span class="required">*</span> обязательны для заполнения.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'itemName'); ?>
		<?php echo $form->textField($model,'itemName',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'itemName'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'itemNameDec'); ?>
		<?php echo $form->textField($model,'itemNameDec',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'itemNameDec'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->