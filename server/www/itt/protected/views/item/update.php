<?php
/* @var $this ItemController */
/* @var $model Item */

$this->breadcrumbs=array(
	'Изделия'=>array('index'),
	$model->iditem=>array('view','id'=>$model->iditem),
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Справочник изделий', 'url'=>array('index')),
	array('label'=>'Добавить изделие', 'url'=>array('create')),
	array('label'=>'Просмотр изделия', 'url'=>array('view', 'id'=>$model->iditem)),
);
?>

<h1>Редактирование изделия <?php echo $model->iditem; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>