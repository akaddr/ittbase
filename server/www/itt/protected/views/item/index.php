<?php
/* @var $this ItemController */
/* @var $model Item */

$this->breadcrumbs=array(
    'Изделия'=>array('index'),
    'Справочник изделий',
);

$this->menu=array(
    array('label'=>'Создать новое изделие', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#item-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php echo CHtml::link('Расширенный поиск','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search',array(
        'model'=>$model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'item-grid',
    'dataProvider'=>$model->search(),
    'cssFile' => Yii::app()->baseUrl . '/css/gridview/gridview.css',
    'filter'=>$model,
    'columns'=>array(
        'iditem',
        'itemName',
        'itemNameDec',
        array(
            'class'=>'CButtonColumn',
        ),
    ),
)); ?>
