<?php
/* @var $this ItemController */
/* @var $model Item */

$this->breadcrumbs=array(
	'Изделия'=>array('index'),
	'Создание',
);

$this->menu=array(
	array('label'=>'Список изделий', 'url'=>array('index')),
);
?>

<h1>Создание изделия</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>