<?php
/* @var $this ItemController */
/* @var $model Item */

$this->breadcrumbs=array(
	'Изделия'=>array('index'),
	$model->iditem,
);

$this->menu=array(
	array('label'=>'Справочник изделий', 'url'=>array('index')),
	array('label'=>'Создать изделие', 'url'=>array('create')),
	array('label'=>'Редатикровать изделие', 'url'=>array('update', 'id'=>$model->iditem)),
	array('label'=>'Удалить изделие', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->iditem),'confirm'=>'Вы действительно хотите удалить изделие?')),
);
?>

<h1>Просмотр изделия с ID <?php echo $model->iditem; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'iditem',
		'itemName',
		'itemNameDec',
	),
)); ?>
