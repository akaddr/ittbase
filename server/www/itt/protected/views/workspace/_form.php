<?php
/* @var $this WorkspaceController */
/* @var $model Workspace */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'workspace-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Поля со звёздочкой <span class="required">*</span> обязательны для заполнения.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'workspaceName'); ?>
		<?php echo $form->textField($model,'workspaceName',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'workspaceName'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'workspaceNumber'); ?>
		<?php echo $form->textField($model,'workspaceNumber',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'workspaceNumber'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'workspaceMisc'); ?>
		<?php echo $form->textField($model,'workspaceMisc',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'workspaceMisc'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->