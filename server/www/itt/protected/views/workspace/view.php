<?php
/* @var $this WorkspaceController */
/* @var $model Workspace */

$this->breadcrumbs=array(
	'Рабочие места'=>array('index'),
	$model->idworkspace,
);

$this->menu=array(
	array('label'=>'Список рабочих мест', 'url'=>array('index')),
	array('label'=>'Добавить рабочее место', 'url'=>array('create')),
	array('label'=>'Редактировать место', 'url'=>array('update', 'id'=>$model->idworkspace)),
	array('label'=>'Удалить место', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idworkspace),'confirm'=>'Вы точно хотите удалить место?')),
);
?>

<h1>Просмотр рабочего места ID <?php echo $model->idworkspace; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idworkspace',
		'workspaceName',
		'workspaceNumber',
		'workspaceMisc',
	),
)); ?>
