<?php
/* @var $this WorkspaceController */
/* @var $model Workspace */

$this->breadcrumbs=array(
    'Рабочие места'=>array('index'),
    'Список',
);

$this->menu=array(
    array('label'=>'Список рабочих мест', 'url'=>array('index')),
    array('label'=>'Добавление места', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#workspace-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php echo CHtml::link('Расширенный поиск','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search',array(
        'model'=>$model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'workspace-grid',
    'dataProvider'=>$model->search(),
    'cssFile' => Yii::app()->baseUrl . '/css/gridview/gridview.css',
    'filter'=>$model,
    'columns'=>array(
        'idworkspace',
        'workspaceName',
        'workspaceNumber',
        'workspaceMisc',
        array(
            'class'=>'CButtonColumn',
        ),
    ),
)); ?>