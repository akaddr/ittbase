<?php
/* @var $this WorkspaceController */
/* @var $model Workspace */

$this->breadcrumbs=array(
	'Рабочие места'=>array('index'),
	$model->idworkspace=>array('view','id'=>$model->idworkspace),
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Список рабочих мест', 'url'=>array('index')),
	array('label'=>'Добавление места', 'url'=>array('create')),
	array('label'=>'Просмотр места', 'url'=>array('view', 'id'=>$model->idworkspace)),
);
?>

<h1>Редактирование рабочего места ID <?php echo $model->idworkspace; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>