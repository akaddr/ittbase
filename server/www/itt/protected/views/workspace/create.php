<?php
/* @var $this WorkspaceController */
/* @var $model Workspace */

$this->breadcrumbs=array(
	'Рабочие места'=>array('index'),
	'Добавление',
);

$this->menu=array(
	array('label'=>'Список рабочих мест', 'url'=>array('index')),
);
?>

<h1>Добавление рабочего места</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>