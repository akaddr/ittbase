<?php
/* @var $this WorkspaceController */
/* @var $data Workspace */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idworkspace')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idworkspace), array('view', 'id'=>$data->idworkspace)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('workspaceName')); ?>:</b>
	<?php echo CHtml::encode($data->workspaceName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('workspaceNumber')); ?>:</b>
	<?php echo CHtml::encode($data->workspaceNumber); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('workspaceMisc')); ?>:</b>
	<?php echo CHtml::encode($data->workspaceMisc); ?>
	<br />


</div>