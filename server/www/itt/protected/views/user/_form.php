<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Поля со звёздочкой <span class="required">*</span> обязательны для заполнения.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'new_password'); ?>
		<?php echo $form->passwordField($model,'new_password',array('size'=>45,'maxlength'=>45,'value'=>'')); ?>
		<?php echo $form->error($model,'new_password'); ?>
	</div>


    <div class="row">
        <?php echo $form->labelEx($model,'new_confirm'); ?>
        <?php echo $form->passwordField($model,'new_confirm',array('size'=>45,'maxlength'=>45,'value'=>'')); ?>
        <?php echo $form->error($model,'new_confirm'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'useradmin'); ?>
		<?php echo $form->checkBox($model,'useradmin'); ?>
		<?php echo $form->error($model,'useradmin'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->