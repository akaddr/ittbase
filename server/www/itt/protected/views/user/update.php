<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Пользователи'=>array('index'),
	$model->iduser=>array('view','id'=>$model->iduser),
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Список', 'url'=>array('index')),
	array('label'=>'Добавить', 'url'=>array('create')),
	array('label'=>'Просмотр', 'url'=>array('view', 'id'=>$model->iduser)),
);
?>

<h1>Update User <?php echo $model->iduser; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>