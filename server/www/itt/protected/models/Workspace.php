<?php

/**
 * This is the model class for table "workspace".
 *
 * The followings are the available columns in table 'workspace':
 * @property string $idworkspace
 * @property string $workspaceName
 * @property string $workspaceNumber
 * @property string $workspaceMisc
 *
 * The followings are the available model relations:
 * @property Test[] $tests
 */
class Workspace extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'workspace';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('workspaceName, workspaceNumber', 'required'),
			array('workspaceName, workspaceNumber, workspaceMisc', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idworkspace, workspaceName, workspaceNumber, workspaceMisc', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tests' => array(self::HAS_MANY, 'Test', 'testidworkspace'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idworkspace' => 'ID рабочего места',
			'workspaceName' => 'Название рабочего места',
			'workspaceNumber' => 'Номер рабочего места',
			'workspaceMisc' => 'Примечания',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idworkspace',$this->idworkspace,true);
		$criteria->compare('workspaceName',$this->workspaceName,true);
		$criteria->compare('workspaceNumber',$this->workspaceNumber,true);
		$criteria->compare('workspaceMisc',$this->workspaceMisc,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Workspace the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
