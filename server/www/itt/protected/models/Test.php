<?php

/**
 * This is the model class for table "test".
 *
 * The followings are the available columns in table 'test':
 * @property string $idtest
 * @property string $testiditem
 * @property string $testItemNumber
 * @property string $testidworkspace
 *
 * The followings are the available model relations:
 * @property Proc[] $procs
 * @property Item $testiditem0
 * @property Workspace $testidworkspace0
 */
class Test extends CActiveRecord
{
    public $workspace_search;
    public $item_search;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'test';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('testiditem, testidworkspace', 'required'),
			array('testiditem, testidworkspace', 'length', 'max'=>10),
			array('testItemNumber', 'length', 'max'=>45),
            array('testNote', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idtest, testiditem, testItemNumber, testNote, testidworkspace, workspace_search, item_search', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'procs' => array(self::HAS_MANY, 'Proc', 'procidtest'),
			'item' => array(self::BELONGS_TO, 'Item', 'testiditem'),
			'workspace' => array(self::BELONGS_TO, 'Workspace', 'testidworkspace'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idtest' => 'ID испытания',
			'item_search' => 'Изделие',
			'testItemNumber' => 'Номер тестируемого изделия',
			'testidworkspace' => 'ID рабочего места',
            'workspace_search' => 'Рабочее место',
            'testNote' => 'Примечание'            
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
        $criteria->with = array('workspace', 'item');

		$criteria->compare('idtest',$this->idtest,true);
		$criteria->compare('testiditem',$this->testiditem,true);
		$criteria->compare('testItemNumber',$this->testItemNumber,true);
        $criteria->compare('testNote',$this->testItemNumber,true);
		$criteria->compare('testidworkspace',$this->testidworkspace,true);
        $criteria->compare('workspace.workspaceName', $this->workspace_search, true );
        $criteria->compare('item.itemName', $this->item_search, true );

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>50,
            ),
            'sort'=>array(
                'attributes'=>array(
                    'workspace_search'=>array(
                        'asc'=>'workspace.workspaceName',
                        'desc'=>'workspace.workspaceName DESC',
                    ),
                    'item_search'=>array(
                        'asc'=>'item.itemName',
                        'desc'=>'item.itemName DESC',
                    ),
                    '*',
                ),
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Test the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function workspacesOfItem($itemid) {
	    $itemid = (int)$itemid;
	    $criteria=new CDbCriteria;
	    $criteria->select = array('testidworkspace');
	    $criteria->with = array('workspace');
	    $criteria->condition="testiditem=$itemid";
        return CHtml::listData(self::model()->findAll($criteria), 'testidworkspace', 'workspace.workspaceName');
    }
}
