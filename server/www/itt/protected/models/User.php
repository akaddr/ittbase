<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $iduser
 * @property string $username
 * @property string $userpass
 * @property integer $useradmin
 */
class User extends CActiveRecord
{
    public $new_password;
    public $new_confirm;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username', 'required'),
			array('useradmin', 'numerical', 'integerOnly'=>true),
			array('username', 'length', 'max'=>45),
            array('userpass', 'length', 'max'=>60),
            array('new_password', 'length', 'min'=>1, 'allowEmpty'=>true),
            array('new_confirm', 'compare', 'compareAttribute'=>'new_password', 'message'=>'Пароли не совпадают'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('iduser, username, userpass, useradmin', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'iduser' => 'ID пользователя',
			'username' => 'Логин',
			'userpass' => 'Пароль',
			'useradmin' => 'Администратор',
            'new_password' => 'Новый пароль',
            'new_confirm' => 'Подтверждение пароля',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('iduser',$this->iduser);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('userpass',$this->userpass,true);
		$criteria->compare('useradmin',$this->useradmin);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Проверяет соответствие пароля истинному значению
     * @param $password
     * @return bool
     */
    public function validatePassword($password)
    {
        return CPasswordHelper::verifyPassword($password,$this->userpass);
    }

    /**
     * Хеширует пароль по каким-то неведомым алгоритмам
     * @param $password
     * @return string
     */
    public function hashPassword($password)
    {
        return CPasswordHelper::hashPassword($password);
    }

    protected function beforeSave()
    {
        if (parent::beforeSave())
        {
            if ($this->new_password)
            {
                $this->userpass = $this->hashPassword($this->new_password);
            }
            return true;
        }
        else
            return false;
    }

}
