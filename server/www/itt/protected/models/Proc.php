<?php

/**
 * This is the model class for table "proc".
 *
 * The followings are the available columns in table 'proc':
 * @property string $idproc
 * @property string $procidtest
 * @property string $procidparam
 * @property string $procDateTime
 * @property double $procvalue
 * @property string $procnote
 * @property string $procbinary
 * @property string $procMime
 * @property string $procMimeType
 *
 * The followings are the available model relations:
 * @property Param $param
 * @property Test $test
 */
class Proc extends CActiveRecord
{
    public $workspace_search;
    public $workspaceid_search;
    public $param_search;
    public $item_search;
    public $itemnamedec_search;
    public $itemnumber_search;
    public $testNote_search;    
    public $testiditem;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'proc';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('procidtest, procidparam, procDateTime', 'required'),
			array('procvalue', 'numerical'),
			array('procidtest, procidparam', 'length', 'max'=>10),
			array('procnote, procMimeType', 'length', 'max'=>45),
			//array('procbinary, procMime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idproc, procidtest, procidparam, procDateTime, procvalue, procnote,
			        workspace_search, workspaceid_search, param_search, item_search, itemnamedec_search, itemnumber_search,testNote_search, testiditem', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'param' => array(self::BELONGS_TO, 'Param', 'procidparam'),
			'test' => array(self::BELONGS_TO, 'Test', 'procidtest'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idproc' => 'ID измерения',
			'procidtest' => 'ID испытания',
			'procidparam' => 'ID параметра',
			'procDateTime' => 'Дата и время измерения',
			'procvalue' => 'Значение',
			'procnote' => 'Примечания',
//			'procbinary' => 'Бинарные данные',
//			'procMime' => 'Mime',
//			'procMimeType' => 'MimeType',
            'workspace_search' => 'Рабочее место',
            'workspaceid_search' => 'ID рабочего места',
            'param_search' => 'Параметр',
            'item_search' => 'Изделие',
            'itemnamedec_search' => 'Децимальный номер',
            'itemnumber_search' => 'Номер тестируемого изделия',
            'testNote_search' => 'Примечание',
            'procMimeHtml' => 'Бинарник',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;
        $criteria->with = array('test','test.workspace','param', 'test.item');
		$criteria->compare('idproc',$this->idproc,false);
		$criteria->compare('procidtest',$this->procidtest,false);
		$criteria->compare('procidparam',$this->procidparam,false);
		$criteria->compare('procDateTime',$this->procDateTime,true);
		$criteria->compare('procvalue',$this->procvalue);
		$criteria->compare('procnote',$this->procnote,true);
		$criteria->compare('procbinary',$this->procbinary,false);
		$criteria->compare('procMime',$this->procMime,false);
		$criteria->compare('procMimeType',$this->procMimeType,false);
        $criteria->compare('workspace.workspaceName',$this->workspace_search,true);
        $criteria->compare('workspace.idworkspace',$this->workspaceid_search,false);
        $criteria->compare('param.paramName',$this->param_search,true);
        $criteria->compare('item.itemName',$this->item_search,true);
        $criteria->compare('item.itemNameDec',$this->itemnamedec_search,true);
        $criteria->compare('test.testItemNumber',$this->itemnumber_search,true);
        $criteria->compare('test.testNote',$this->testNote_search,true);
        $criteria->compare('testiditem',$this->testiditem,false);

		$criteria->limit = 3;

		$criteria->select = array('idproc','procidtest','procidparam','procDateTime','procvalue','procnote', 'LENGTH(procMime) as procMime');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>50,
            ),
            'sort'=>array(
                'attributes'=>array(
                    'workspace_search'=>array(
                        'asc'=>'workspace.workspaceName',
                        'desc'=>'workspace.workspaceName DESC',
                    ),
                    'item_search'=>array(
                        'asc'=>'item.itemName',
                        'desc'=>'item.itemName DESC',
                    ),
                    'param_search'=>array(
                        'asc'=>'param.paramName',
                        'desc'=>'param.paramName DESC',
                    ),
                    'itemnamedec_search'=>array(
                        'asc' => 'item.itemNameDec',
                        'desc' => 'item.itemNameDec DESC',
                    ),
                    'itemnumber_search'=>array(
                        'asc' => 'test.testItemNumber',
                        'desc' => 'test.testItemNumber DESC',
                    ),
                    'testNote_search'=>array(
                        'asc' => 'test.testNote',
                        'desc' => 'test.testNote DESC',
                    ),                    
                    '*',
                ),
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Proc the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getProcMimeHtml() {
        if ($this->procMime>0) {
            return CHtml::link('Открыть/скачать', array('procmime', 'id'=>$this->idproc), array('target'=>'_blank','class'=>'mime'));
        } else {
            return false;
        }
    }

    public static function paramsOfWorspace($workspaceid) {
	    $workspaceid = (int)$workspaceid;
	    $criteria=new CDbCriteria;
	    $criteria->select = array('procidparam');
	    $criteria->with = array('param','test');
	    $criteria->condition="testidworkspace=$workspaceid";
	    return CHtml::listData(	self::model()->with(array('param','test'))->findAll($criteria), 'procidparam', 'param.paramName');
    }


}
