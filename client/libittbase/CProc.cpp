/*
 * CItems.cpp
 *
 *  Created on: 26 ����� 2014 �.
 *      Author: D
 */

#include <vcl.h>

#include "CProc.h"
#include "ActiveSprFormImpl.h"

#include <list>

CProc::CProc(AnsiString _udl) {
  udl = _udl;
  items = new TList();
  newItems = new TList();
}

CResult CProc::saveItemsToDB(TList *_items, TADOQuery *query) {
  if (_items->Count > 0) {
    query->SQL->Text =
	"INSERT INTO `proc` (`procidtest`, `procidparam`, `procDateTime`, `procvalue`,`procnote`,`procbinary`,`procMime`,`procMimeType`)" " VALUES (:procidtest, :procidparam, now(), :procvalue, :procnote, :procbinary, :procMime, :procMimeType)";
    while (newItems->Count > 0) {
      item = (TProcess*)_items->Items[0];

      query->Parameters->ParamByName("procidtest")->Value =
	  (__int64)item->procidtest;
      query->Parameters->ParamByName("procidparam")->Value =
	  (__int64)item->procidparam;
      query->Parameters->ParamByName("procvalue")->DataType = ftFloat;
      query->Parameters->ParamByName("procvalue")->Value = item->procvalue;
      query->Parameters->ParamByName("procnote")->Value = item->procnote;
      if (FileExists(item->procbinary))
	query->Parameters->ParamByName("procbinary")->LoadFromFile
	    (item->procbinary, ftBlob);
      else
	query->Parameters->ParamByName("procbinary")->Value = "";
      if (FileExists(item->procMime))
	query->Parameters->ParamByName("procMime")->LoadFromFile(item->procMime,
	  ftBlob);
      else
	query->Parameters->ParamByName("procMime")->Value = "";
      query->Parameters->ParamByName("procMimeType")->Value =
	  item->procMimeType;

      if (query->ExecSQL() == 1)
	_items->Delete(0);
      else
	break;
    }
  }
  return CResult(ERR_NOERROR);
}

CResult CProc::loadItemsFromDB(TList *_items, TADOQuery *query) {
  query->SQL->Text = "select * from proc LIMIT 100";
  query->Open();
  if (query->RecordCount > 0) {
    _items->Clear();
    query->First();
    while (!query->Eof) {
      item = new TProcess;

      item->idproc = query->Fields->FieldByName("idproc")->AsFloat;
      item->procidtest = query->Fields->FieldByName("procidtest")->AsFloat;
      item->procidparam = query->Fields->FieldByName("procidparam")->AsFloat;
      item->procDateTime = query->Fields->FieldByName("procDateTime")->AsString;
      item->procvalue = query->Fields->FieldByName("procvalue")->AsFloat;
      item->procnote = query->Fields->FieldByName("procnote")->AsString;
      item->procMimeType = query->Fields->FieldByName("procMimeType")->AsString;

      _items->Add(item);
      query->Next();
    }
    query->Close();
    return CResult(ERR_NOERROR);
  }
  else
    return CResult(ERR_NOERROR);
}

AnsiString CProc::getItemCaption(int i) {
  item = (TProcess*)items->Items[i];

  return DateToStr(item->procDateTime) + " " + item->procvalue;
}

long CProc::getItemId(int i) {
  item = (TProcess*)items->Items[i];

  return item->idproc;
}

CResult CProc::addNewItem(TProcess *_item) {
  item = new TProcess;
  *item = *_item;
  newItems->Add(item);
  return CResult(ERR_NOERROR);
}

CResult CProc::getItemByIndex(uint64_t i, TProcess *item) {
  if (i < items->Count) {
    *item = *(TProcess*)items->Items[i];
    return CResult(ERR_NOERROR);
  }
  return CResult(ERR_READITEMS);
}

CResult CProc::getItemById(uint64_t id, TProcess *item) {
  if (items->Count > 0) {
    for (uint64_t i = 0; i < items->Count; i++)
      if (getItemId(i) == id) {
	*item = *(TProcess*)items->Items[i];
	return CResult(ERR_NOERROR);
      }
  }
  return CResult(ERR_READITEMS);
}

CResult CProc::getItemByGui(TProcess *item) {
  TActiveSprForm *OKBottomDlg;

  if (items->Count > 0) {
    OKBottomDlg = new TActiveSprForm(NULL);
    OKBottomDlg->ComboBox->Items->Clear();
    OKBottomDlg->Caption = "�����";

    for (uint64_t i = 0; i < items->Count; i++)
      OKBottomDlg->ComboBox->Items->Add(getItemCaption(i));

    OKBottomDlg->ComboBox->ItemIndex = 0;
    OKBottomDlg->ShowModal();

    if (OKBottomDlg->ModalResult == mrOk) {
      *item = *(TProcess*)items->Items[OKBottomDlg->ComboBox->ItemIndex];
      return CResult(ERR_NOERROR);
    }
  }
  return CResult(ERR_READITEMS);
}

CResult CProc::sync() {
  TADOQuery *query = new TADOQuery(0);
  query->ConnectionString = udl;
  CResult r;

  if (query->Active)
    query->Close();

  if (newItems->Count > 0) {
    r = saveItemsToDB(newItems, query);
    if (r.noerror())
      newItems->Clear();
  }

  if (query->Active)
    query->Close();

  // r = loadItemsFromDB(items, query);

  delete query;
  return r;
}
