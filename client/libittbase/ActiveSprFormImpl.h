// ----------------------------------------------------------------------------
#ifndef ActiveSprFormImplH
#define ActiveSprFormImplH
// ----------------------------------------------------------------------------
// #include <OKCANCL1.h>
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
#include <StdCtrls.hpp>
#include <Controls.hpp>
#include <Forms.hpp>
#include <Graphics.hpp>
#include <Classes.hpp>
#include <SysUtils.hpp>
#include <Windows.hpp>
#include <System.hpp>

// ----------------------------------------------------------------------------
class TActiveSprForm : public TForm {
__published:
  TPanel *Panel1;
  TPanel *Panel2;
  TButton *Button2;
  TButton *Button1;
  TComboBox *ComboBox;

  void __fastcall HelpBtnClick(TObject *Sender);

private:
public:
  virtual __fastcall TActiveSprForm(TComponent* AOwner);
};

// ----------------------------------------------------------------------------
extern PACKAGE TActiveSprForm *ActiveSprForm;
// ----------------------------------------------------------------------------
#endif
