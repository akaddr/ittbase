/*
 * CParams.h
 *
 *  Created on: 24 ����� 2014 �.
 *      Author: D
 */

#ifndef CSYSTEM_H_
#define CSYSTEM_H_

#include <vcl.h>
#include <ADODB.hpp>
                       
#include "CResult.h" 

typedef struct
{
   uint64_t        version;
} TSystem;

class CSystem
{
        AnsiString udl;
        TSystem system;
public:
        CSystem(AnsiString _udl);
        CResult sync();
        uint64_t getVersion();
};

#endif /* CSYSTEM_H_ */
