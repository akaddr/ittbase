/*
 * CITTBase.cpp
 *
 *  Created on: 05 ����� 2014 �.
 *      Author: D
 */
#include <vcl.h>

#include "CITTBase.h"
#include "ActiveAddTestDialogImpl.h"
#include <Registry.hpp>

#include <idGlobal.hpp>
#include <IdGlobalProtocols.hpp>

CITTBase::CITTBase() {

  CResult r;
  char str[1024];
  lastErrorCode = 0;

  DBString = loadDataStringFromReg();

  if (DBString == "") {
    resetConnection();
  }

  items = new CItems(DBString);
  params = new CParams(DBString);
  system = new CSystem(DBString);
  workspace = new CWorkspace(DBString);
  test = new CTest(DBString);
  proc = new CProc(DBString);

  curitem.iditem = -1;
  curitem.itemName = "";
  curitem.itemNameDec = "";

  curworkspace.idworkspace = -1;
  curworkspace.workspaceName = "";
  curworkspace.workspaceNumber = "";
  curworkspace.workspaceMisc = "";

  curtest.idtest = -1;
  curtest.testiditem = -1;
  curtest.testItemNumber = "";
  curtest.testidworkspace = -1;
  curtest.testNote = "";
}

CResult CITTBase::resetConnection() {
  DBString = PromptDataSource(NULL, "");
  if (DBString != "")
    saveDataStringToReg(DBString);

  return CResult(ERR_NOERROR);
}

CResult CITTBase::sync() {
  try {
    lastErrorCode = 0;

    if (system->sync().noerror()) {
      if (system->getVersion() == BASEVERSION) {
	if (items->sync().noerror() && params->sync().noerror() && system->sync
	    ().noerror() && workspace->sync().noerror() && test->sync()
	    .noerror())
	  return CResult(lastErrorCode = ERR_NOERROR);
	else
	  CResult(lastErrorCode = ERR_SYNCBASE);
      }
      else
	return CResult(lastErrorCode = ERR_BASEVERSION);
    }
    else
      return CResult(lastErrorCode = ERR_READSYSTEM);
  }
  catch (...) {
    lastErrorCode = ERR_SYNCBASE;
  }
  return CResult(lastErrorCode);
}

CResult CITTBase::selectItemByGui() {
  lastErrorCode = items->getItemByGui(&curitem).getResultCode();
  return CResult(lastErrorCode);
}

CResult CITTBase::selectItemById(__int64 id) {
  lastErrorCode = items->getItemById(id, &curitem).getResultCode();
  return CResult(lastErrorCode);
}

AnsiString CITTBase::getItemCaption() {
  return curitem.itemName + " " + curitem.itemNameDec;
}

CResult CITTBase::selectWorkspaceByGui() {
  lastErrorCode = workspace->getItemByGui(&curworkspace).getResultCode();
  return CResult(lastErrorCode);
}

CResult CITTBase::selectWorkspaceById(__int64 id) {
  lastErrorCode = workspace->getItemById(id, &curworkspace).getResultCode();
  return CResult(lastErrorCode);
}

AnsiString CITTBase::getWorkspaceCaption() {
  return curworkspace.workspaceName + " �" + curworkspace.workspaceNumber;
}

CResult CITTBase::selectTestByGui() {
  lastErrorCode = test->getItemByGui(&curtest).getResultCode();
  return CResult(lastErrorCode);
}

CResult CITTBase::selectTestById(__int64 id) {
  lastErrorCode = test->getItemById(id, &curtest).getResultCode();
  return CResult(lastErrorCode);
}

AnsiString CITTBase::getTestCaption() {
  return curtest.testNote + " �" + curtest.testItemNumber;
}

CResult CITTBase::newTest(AnsiString testItemNumber, AnsiString testNote) {
  if ((curworkspace.idworkspace == -1) || (curitem.iditem == -1))
    return CResult(lastErrorCode = ERR_ADDTEST);

  curtest.idtest = 0;
  curtest.testiditem = curitem.iditem;
  curtest.testItemNumber = testItemNumber;
  curtest.testidworkspace = curworkspace.idworkspace;
  curtest.testNote = testNote;

  if (test->addNewItem(&curtest).noerror()) {
    if (test->sync().noerror()) {
      curtest.idtest = getLastId();
      if (curtest.idtest != -1)
	return CResult(lastErrorCode = ERR_NOERROR);
    }
    else
      return CResult(lastErrorCode = ERR_ADDTEST);
  }
  else
    return CResult(lastErrorCode = ERR_ADDTEST);
  return 0;
}

CResult CITTBase::newTestByGui(AnsiString testItemNumber, AnsiString testNote) {
  TActiveAddTestDialog *dialogAddTest = new TActiveAddTestDialog(NULL);

  dialogAddTest->Caption = "����� ���������";
  dialogAddTest->cbItem->Items->Clear();
  for (long i = 0; i < items->getItemsCount(); i++)
    dialogAddTest->cbItem->Items->Add(items->getItemCaption(i));

  dialogAddTest->cbWorkPlace->Items->Clear();
  for (long i = 0; i < workspace->getItemsCount(); i++)
    dialogAddTest->cbWorkPlace->Items->Add(workspace->getItemCaption(i));

  if (loadDataStringFromReg("ItemIndex") != "")
    dialogAddTest->cbItem->ItemIndex =
	loadDataStringFromReg("ItemIndex").ToInt();

  if (loadDataStringFromReg("WorkspaceIndex") != "")
    dialogAddTest->cbWorkPlace->ItemIndex =
	loadDataStringFromReg("WorkspaceIndex").ToInt();

  dialogAddTest->editItemNumber->Text = testItemNumber;
  dialogAddTest->editTestNote->Text = testNote;

  dialogAddTest->ShowModal();

  if (dialogAddTest->ModalResult == mrOk) {
    items->getItemByIndex(dialogAddTest->cbItem->ItemIndex, &curitem);
    workspace->getItemByIndex(dialogAddTest->cbWorkPlace->ItemIndex,
	&curworkspace);

    try {
      saveDataStringToReg("ItemIndex", dialogAddTest->cbItem->ItemIndex);
      saveDataStringToReg("WorkspaceIndex",
	  dialogAddTest->cbWorkPlace->ItemIndex);
    }
    catch (...) {
    }

    return newTest(dialogAddTest->editItemNumber->Text,
	dialogAddTest->editTestNote->Text);
  }
  else
    return CResult(lastErrorCode = ERR_NOERROR);
}

CResult CITTBase::addProcessValue(AnsiString param_name, double procvalue,
    AnsiString procnote, AnsiString filebinary, AnsiString fileMime) {

  TPar param;
  if (params->getParamByName(param_name, &param).noerror()) {
    return addProcessValue(param.idparam, procvalue, procnote, filebinary,
	fileMime);
  }
  else {
    param.idparam = 0;
    param.paramName = param_name;
    param.paramNote = "";
    params->addNewParam(&param);
    params->sync();
    sync();
    params->getParamByName(param_name, &param);
  }
  return addProcessValue(param.idparam, procvalue, procnote, filebinary,
      fileMime);

}

CResult CITTBase::addProcessValue(uint64_t procidparam, double procvalue,
    AnsiString procnote, AnsiString filebinary, AnsiString fileMime) {
  CResult result;
  TProcess p;
  TIdMimeTable *tab;

  if ((curtest.idtest == -1) || (procidparam == -1))
    return CResult(lastErrorCode = ERR_ADDPARAM);

  p.idproc = 0;
  p.procidtest = curtest.idtest;
  p.procidparam = procidparam;
  p.procDateTime = Now();
  p.procvalue = procvalue;
  p.procnote = procnote;
  p.procbinary = filebinary;
  p.procMime = fileMime;

  if (FileExists(fileMime)) {
    tab = new TIdMimeTable(true);
    p.procMimeType = tab->GetFileMIMEType(fileMime);
    delete tab;
  }
  else
    p.procMimeType = "";

  if (proc->addNewItem(&p).noerror()) {
    if (proc->sync().noerror()) {
      p.idproc = getLastId();
      if (p.idproc != -1)
	return CResult(lastErrorCode = ERR_NOERROR);
    }
    else
      return CResult(lastErrorCode = ERR_ADDPARAM);

  }
  else
    return CResult(lastErrorCode = ERR_ADDPARAM);

  return CResult(lastErrorCode = ERR_NOERROR);

  return result;
}

CITTBase::~CITTBase() {
  // TODO Auto-generated destructor stub
}

long CITTBase::getLastId() {
  long result;

  TADOQuery *query = new TADOQuery(0);
  query->ConnectionString = DBString;

  query->SQL->Text = "SELECT LAST_INSERT_ID()";
  query->Open();
  if (query->RecordCount > 0) {
    result = query->Fields->FieldByName("LAST_INSERT_ID()")->AsFloat;
    delete query;
    return result;
  }
  else
    return -1;
}

AnsiString CITTBase::getDBString() {
  return DBString;
}

CResult CITTBase::getProcMimeByIdToFile(__int64 id, AnsiString filename) {
  if (filename == "")
    return CResult(lastErrorCode = ERR_READPROCBINARY);

  TADOQuery *query = new TADOQuery(0);

  query->ConnectionString = DBString;

  query->SQL->Text = "SELECT procMime FROM proc WHERE idproc=" +
      IntToStr((__int64)id);
  query->Open();
  if (query->RecordCount > 0) {
    TFileStream *f = new TFileStream(filename, fmCreate);
    query->First();

    TStream *s = query->CreateBlobStream(query->FieldByName("procMime"),
	bmRead);
    f->CopyFrom(s, s->Size);

    query->Close();
    delete query;
    delete f;
    return CResult(lastErrorCode = ERR_NOERROR);
  }
  else
    return CResult(lastErrorCode = ERR_READPROCBINARY);
}

CResult CITTBase::getProcBinaryByIdToFile(__int64 id, AnsiString filename) {
  if (filename == "")
    return CResult(lastErrorCode = ERR_READPROCBINARY);

  TADOQuery *query = new TADOQuery(0);

  query->ConnectionString = DBString;

  query->SQL->Text = "SELECT procbinary FROM proc WHERE idproc=" +
      IntToStr((__int64)id);
  query->Open();
  if (query->RecordCount > 0) {
    TFileStream *f = new TFileStream(filename, fmCreate);
    query->First();

    TStream *s = query->CreateBlobStream(query->FieldByName("procbinary"),
	bmRead);
    f->CopyFrom(s, s->Size);

    query->Close();
    delete query;
    delete f;
    return CResult(lastErrorCode = ERR_NOERROR);
  }
  else
    return CResult(lastErrorCode = ERR_READPROCBINARY);
}

AnsiString CITTBase::getLastResult(__int64 *code) {
  *code = lastErrorCode;
  return CResult(lastErrorCode).getResultText();
}

void CITTBase::saveDataStringToReg(AnsiString datastr) {
  saveDataStringToReg("DBString", datastr);
}

void CITTBase::saveDataStringToReg(AnsiString key, AnsiString datastr) {
  TRegistry* reg = new TRegistry();
  reg->RootKey = HKEY_CURRENT_USER;
  reg->Access = KEY_WRITE;

  bool openResult = reg->OpenKey("Software\\ITT\\ActiveBase\\", true);

  if (!openResult) {
    MessageDlg("Unable to create key! Exiting.", mtError,
	TMsgDlgButtons() << mbOK, 0);
    return;
  }

  reg->WriteString(key, datastr);

  reg->CloseKey();
  reg->Free();
}

AnsiString CITTBase::loadDataStringFromReg() {
  return loadDataStringFromReg("DBString");
}

AnsiString CITTBase::loadDataStringFromReg(AnsiString key) {
  AnsiString result = "";

  TRegistry* reg = new TRegistry();
  reg->RootKey = HKEY_CURRENT_USER;
  reg->Access = KEY_READ;

  bool openResult = reg->OpenKey("Software\\ITT\\ActiveBase\\", true);

  if (!openResult) {
    MessageDlg("Unable to Read key! Exiting.", mtError,
	TMsgDlgButtons() << mbOK, 0);
  }
  else {
    result = reg->ReadString(key);
    reg->CloseKey();
  }

  reg->Free();

  return result;
}
