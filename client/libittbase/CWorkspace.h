/*
 * CWorkSpace.h
 *
 *  Created on: 25 ����� 2014 �.
 *      Author: D
 */

#ifndef CWORKSPACE_H_
#define CWORKSPACE_H_

#include <vcl.h>
#include <ADODB.hpp>
                       
#include "CResult.h"
                
typedef struct
{
        uint64_t idworkspace;
        AnsiString workspaceName;
        AnsiString workspaceNumber;
        AnsiString workspaceMisc;
} TWorkspace;

class CWorkspace
{
        AnsiString udl;
        TList *items;
        TList *newItems;
        TWorkspace *item;

public:
        CWorkspace(AnsiString _udl);
        CResult getItemByIndex(uint64_t i, TWorkspace *_item);
        CResult getItemById(uint64_t id, TWorkspace *_item);
        CResult getItemByGui(TWorkspace *item);
        CResult saveItemsToDB(TList *_items, TADOQuery *query);
        CResult loadItemsFromDB(TList *_items, TADOQuery *query);
        AnsiString getItemCaption(int i);
        long getItemId(int i);
        CResult addNewItem(TWorkspace *_item);
        CResult sync();
        long getItemsCount();

};

#endif /* CWORKSPACE_H_ */
