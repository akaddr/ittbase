// ---------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "ActiveAddTestDialogImpl.h"
// ---------------------------------------------------------------------
#pragma resource "*.dfm"
TActiveAddTestDialog *ActiveAddTestDialog;

// ---------------------------------------------------------------------
__fastcall TActiveAddTestDialog::TActiveAddTestDialog(TComponent* AOwner)
    : TForm(Owner) {
}

// ---------------------------------------------------------------------
void __fastcall TActiveAddTestDialog::HelpBtnClick(TObject *Sender) {
  Application->HelpContext(HelpContext);
}
// ---------------------------------------------------------------------
