/*
 * CParams.cpp
 *
 *  Created on: 18 ����� 2014 �.
 *      Author: D
 */

#include "CParams.h"
#include "ActiveSprFormImpl.h"

CParams::CParams(AnsiString _udl) {
  udl = _udl;
  params = new TList();
  newParams = new TList();
}

CParams::~CParams() {

}

CResult CParams::sync() {
  TADOQuery *query = new TADOQuery(0);
  query->ConnectionString = udl;
  CResult r;

  if (query->Active)
    query->Close();

  if (newParams->Count > 0) {
    r = saveParamsToDB(newParams, query);
    if (r.noerror())
      newParams->Clear();
  }

  if (query->Active)
    query->Close();

  r = loadParamsFromDB(params, query);

  delete query;
  return r;
}

CResult CParams::getParamByIndex(uint64_t i, TPar *Param) {
  if (i < params->Count) {
    *Param = *(TPar*)params->Items[i];
    return CResult(ERR_NOERROR);
  }
  return CResult(ERR_READPARAM);
}

CResult CParams::getParamById(uint64_t id, TPar *Param) {
  if (params->Count > 0) {
    for (uint64_t i = 0; i < params->Count; i++) {
      if ((*(TPar*)params->Items[i]).idparam == id) {
	*Param = *(TPar*)params->Items[i];
	return CResult(ERR_NOERROR);
      }
    }
  }
  return CResult(ERR_READPARAM);
}

CResult CParams::getParamByName(AnsiString name, TPar *Param) {
  if (params->Count > 0) {
    for (uint64_t i = 0; i < params->Count; i++) {
      if ((*(TPar*)params->Items[i]).paramName == name) {
	*Param = *(TPar*)params->Items[i];
	return CResult(ERR_NOERROR);
      }
    }
  }
  return CResult(ERR_READPARAM);
}

CResult CParams::getParamByGui(TPar *Param) {
  TPar it;
  TActiveSprForm *OKBottomDlg;

  if (params->Count > 0) {
    OKBottomDlg = new TActiveSprForm(NULL);
    OKBottomDlg->ComboBox->Items->Clear();
    OKBottomDlg->Caption = "�����";
    for (uint64_t i = 0; i < params->Count; i++) {
      it = *(TPar*)params->Items[i];
      OKBottomDlg->ComboBox->Items->Add(it.paramName);
    }
    OKBottomDlg->ComboBox->ItemIndex = 0;
    OKBottomDlg->ShowModal();
    if (OKBottomDlg->ModalResult == mrOk) {
      it = *(TPar*)params->Items[OKBottomDlg->ComboBox->ItemIndex];
      *Param = it;
      return CResult(ERR_NOERROR);
    }
  }
  return CResult(ERR_READPARAM);
}

CResult CParams::addNewParam(TPar *_param) {
  param = new TPar;
  *param = *_param;
  newParams->Add(param);
  return CResult(ERR_NOERROR);
}

CResult CParams::saveParamsToDB(TList *_params, TADOQuery *query) {
  if (_params->Count > 0) {
    query->SQL->Text =
	"INSERT INTO `param` (`paramName`, `paramNote`) VALUES (:ParamName, :ParamNote)";
    while (_params->Count > 0) {
      param = (TPar*)_params->Items[0];
      query->Parameters->ParamByName("ParamName")->Value = param->paramName;
      query->Parameters->ParamByName("ParamNote")->Value = param->paramNote;
      if (query->ExecSQL() == 1)
	_params->Delete(0);
      else
	break;
    }
  }
  return CResult(ERR_NOERROR);
}

CResult CParams::loadParamsFromDB(TList *_params, TADOQuery *query) {
  query->SQL->Text = "select * from param";
  query->Open();
  if (query->RecordCount > 0) {
    _params->Clear();
    query->First();
    while (!query->Eof) {
      param = new TPar();
      param->idparam = query->Fields->FieldByName("idParam")->AsFloat;
      param->paramName = query->Fields->FieldByName("paramName")->AsString;
      param->paramNote = query->Fields->FieldByName("paramNote")->AsString;

      _params->Add(param);
      query->Next();
    }
    query->Close();
    return CResult(ERR_NOERROR);
  }
  else
    return CResult(ERR_NOERROR);
}

AnsiString CParams::getParamCaption(int i) {
  param = (TPar*)params->Items[i];

  return IntToStr((__int64)param->idparam) + " " + param->paramName;
}

long CParams::getParamId(int i) {
  param = (TPar*)params->Items[i];

  return param->idparam;
}
