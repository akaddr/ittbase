/*
 *  CITTBase.h
 *
 *  Created on: 05 ����� 2014 �.
 *      Author: D
 */

#ifndef CITTBASE_H_
#define CITTBASE_H_

#include <system.hpp>
#include <ADODB.hpp>

#include "CResult.h"
#include "CItems.h"
#include "CParams.h"
#include "CSystem.h"
#include "CWorkspace.h"
#include "CTest.h"
#include "CProc.h"

#define BASEVERSION 2
                 
typedef struct
{
        uint64_t        iduser;
        AnsiString      username;
        AnsiString      userpass;
        bool            useradmin;
} TUser;


class CITTBase {
		AnsiString      DBString;
		TADOQuery       *query;
		TItem curitem;
		TWorkspace curworkspace;
		TTest curtest;
		BSTR* ItemCaption;
		__int64 lastErrorCode;


public:
// temprary public
        CItems          *items;
        CParams         *params;
        CSystem         *system;
        CWorkspace      *workspace;
        CTest           *test;
        CProc           *proc;
        
////////////////////////////////////////
        long getLastId();

        CResult selectItemByGui();                                        //
        CResult selectItemById(__int64 id);                               //
        AnsiString getItemCaption();                                      //

        CResult selectWorkspaceByGui();                                   //
        CResult selectWorkspaceById(__int64 id);                          //
        AnsiString getWorkspaceCaption();                                 //


        CResult selectTestByGui();                                        //
        CResult selectTestById(__int64 id);                               //
        AnsiString getTestCaption();                                      //

        CResult newTest(AnsiString testItemNumber, AnsiString testNote);  //

        CResult newTestByGui(AnsiString testItemNumber="", AnsiString testNote="");                                           //

        CResult addProcessValue(uint64_t       procidparam,
								double         procvalue,
								AnsiString     procnote,
								AnsiString     filebinary,
								AnsiString     fileMime);

        CResult addProcessValue(AnsiString param_name, double procvalue,
	        AnsiString procnote, AnsiString filebinary, AnsiString fileMime);

	AnsiString getDBString();

	CResult getProcBinaryByIdToFile(__int64 id, AnsiString filename);
	CResult getProcMimeByIdToFile(__int64 id, AnsiString filename);
	AnsiString getLastResult(__int64 *code);


	CResult sync();
	CResult resetConnection();

	void saveDataStringToReg(AnsiString datastr);
	void saveDataStringToReg(AnsiString key, AnsiString datastr);

	AnsiString  loadDataStringFromReg();
	AnsiString  loadDataStringFromReg(AnsiString key);

	CITTBase();

	virtual ~CITTBase();
};

#endif /* CITTBASE_H_ */
