// ---------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "ActiveSprFormImpl.h"
// ---------------------------------------------------------------------
#pragma resource "*.dfm"
TActiveSprForm *ActiveSprForm;

// ---------------------------------------------------------------------
__fastcall TActiveSprForm::TActiveSprForm(TComponent* AOwner) : TForm(AOwner) {
}

// ---------------------------------------------------------------------
void __fastcall TActiveSprForm::HelpBtnClick(TObject *Sender) {
  Application->HelpContext(HelpContext);
}
// ---------------------------------------------------------------------
