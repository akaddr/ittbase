object ActiveAddTestDialog: TActiveAddTestDialog
  Left = 575
  Top = 245
  ClientHeight = 355
  ClientWidth = 293
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 293
    Height = 309
    Align = alClient
    Color = clCream
    TabOrder = 0
    DesignSize = (
      293
      309)
    object Label2: TLabel
      Left = 10
      Top = 16
      Width = 71
      Height = 19
      Caption = #1048#1079#1076#1077#1083#1080#1077
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label3: TLabel
      Left = 8
      Top = 80
      Width = 125
      Height = 19
      Caption = #1056#1072#1073#1086#1095#1077#1077' '#1084#1077#1089#1090#1086
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label1: TLabel
      Left = 8
      Top = 144
      Width = 128
      Height = 19
      Caption = #1053#1086#1084#1077#1088' '#1080#1079#1076#1077#1083#1080#1103
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object TLabel
      Left = 8
      Top = 208
      Width = 104
      Height = 19
      Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object cbItem: TComboBox
      Left = 8
      Top = 40
      Width = 270
      Height = 27
      Anchors = [akLeft, akTop, akRight]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object cbWorkPlace: TComboBox
      Left = 8
      Top = 104
      Width = 270
      Height = 27
      Anchors = [akLeft, akTop, akRight]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object editItemNumber: TEdit
      Left = 8
      Top = 168
      Width = 270
      Height = 27
      Anchors = [akLeft, akTop, akRight]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object editTestNote: TEdit
      Left = 8
      Top = 232
      Width = 270
      Height = 27
      Anchors = [akLeft, akTop, akRight]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 309
    Width = 293
    Height = 46
    Align = alBottom
    Color = clCream
    TabOrder = 1
    DesignSize = (
      293
      46)
    object Button2: TButton
      Left = 8
      Top = 12
      Width = 89
      Height = 25
      Caption = #1044#1072
      ModalResult = 1
      TabOrder = 0
    end
    object Button1: TButton
      Left = 196
      Top = 12
      Width = 89
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
    end
  end
end
