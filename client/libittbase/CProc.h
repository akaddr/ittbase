/*
 * CParams.h
 *
 *  Created on: 10 ����� 2014 �.
 *      Author: D
 */

#ifndef CPROC_H_
#define CPROC_H_

#include <vcl.h>
#include <ADODB.hpp>
                       
#include "CResult.h" 

typedef struct
{
   uint64_t             idproc;
   uint64_t             procidtest;
   uint64_t             procidparam;
   TDateTime            procDateTime;
   double               procvalue;
   AnsiString           procnote;
   AnsiString           procbinary;
   AnsiString 			procMime;
   AnsiString           procMimeType;
} TProcess;

class CProc
{
        AnsiString udl;
        TList *items;
        TList *newItems;
		TProcess *item;

public:
        CProc(AnsiString _udl);
		CResult getItemByIndex(uint64_t i, TProcess *_item);
		CResult getItemById(uint64_t id, TProcess *_item);
		CResult getItemByGui(TProcess *_item);
        CResult saveItemsToDB(TList *_items, TADOQuery *query);
        CResult loadItemsFromDB(TList *_items, TADOQuery *query);
        AnsiString getItemCaption(int i);
        long getItemId(int i);
        CResult addNewItem(TProcess *_item);
        CResult sync();
};

#endif /* CPROC_H_ */
