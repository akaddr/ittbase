/*
 * CWorkspace.cpp
 *
 *  Created on: 25 ����� 2014 �.
 *      Author: D
 */

#include <vcl.h>

#include "CWorkspace.h"
#include "ActiveSprFormImpl.h"

CWorkspace::CWorkspace(AnsiString _udl) {
  udl = _udl;
  items = new TList();
  newItems = new TList();
}

CResult CWorkspace::saveItemsToDB(TList *_items, TADOQuery *query) {
  if (_items->Count > 0) {
    query->SQL->Text =
	"INSERT INTO `workspace` (`workspaceName`, `workspaceNumber`, `workspaceMisc`) VALUES (:workspaceName, :workspaceNumber, :workspaceMisc)";
    while (newItems->Count > 0) {
      item = (TWorkspace*)_items->Items[0];
      query->Parameters->ParamByName("workspaceName")->Value =
	  item->workspaceName;
      query->Parameters->ParamByName("workspaceNumber")->Value =
	  item->workspaceNumber;
      query->Parameters->ParamByName("workspaceMisc")->Value =
	  item->workspaceMisc;
      if (query->ExecSQL() == 1)
	_items->Delete(0);
      else
	break;
    }
  }
  return CResult(ERR_NOERROR);
}

CResult CWorkspace::loadItemsFromDB(TList *_items, TADOQuery *query) {
  query->SQL->Text = "select * from workspace";
  query->Open();
  if (query->RecordCount > 0) {
    _items->Clear();
    query->First();
    while (!query->Eof) {
      item = new TWorkspace;
      item->idworkspace = query->Fields->FieldByName("idworkspace")->AsFloat;
      item->workspaceName = query->Fields->FieldByName("workspaceName")
	  ->AsString;
      item->workspaceNumber = query->Fields->FieldByName("workspaceNumber")
	  ->AsString;
      item->workspaceMisc = query->Fields->FieldByName("workspaceMisc")
	  ->AsString;

      _items->Add(item);
      query->Next();
    }
    query->Close();
    return CResult(ERR_NOERROR);
  }
  else
    return CResult(ERR_NOERROR);
}

AnsiString CWorkspace::getItemCaption(int i) {
  item = (TWorkspace*)items->Items[i];

  return item->workspaceName + " �" + item->workspaceNumber;
}

long CWorkspace::getItemId(int i) {
  item = (TWorkspace*)items->Items[i];

  return item->idworkspace;
}

CResult CWorkspace::addNewItem(TWorkspace *_item) {
  item = new TWorkspace;
  *item = *_item;
  newItems->Add(item);
  return CResult(ERR_NOERROR);
}

CResult CWorkspace::getItemByIndex(uint64_t i, TWorkspace *item) {
  if (i < items->Count) {
    *item = *(TWorkspace*)items->Items[i];
    return CResult(ERR_NOERROR);
  }
  return CResult(ERR_READITEMS);
}

CResult CWorkspace::getItemById(uint64_t id, TWorkspace *item) {
  if (items->Count > 0) {
    for (uint64_t i = 0; i < items->Count; i++)
      if (getItemId(i) == id) {
	*item = *(TWorkspace*)items->Items[i];
	return CResult(ERR_NOERROR);
      }
  }
  return CResult(ERR_READITEMS);
}

CResult CWorkspace::getItemByGui(TWorkspace *item) {
  TActiveSprForm *OKBottomDlg;

  if (items->Count > 0) {
    OKBottomDlg = new TActiveSprForm(NULL);
    OKBottomDlg->ComboBox->Items->Clear();
    OKBottomDlg->Caption = "�����";

    for (uint64_t i = 0; i < items->Count; i++)
      OKBottomDlg->ComboBox->Items->Add(getItemCaption(i));

    OKBottomDlg->ComboBox->ItemIndex = 0;
    OKBottomDlg->ShowModal();

    if (OKBottomDlg->ModalResult == mrOk) {
      *item = *(TWorkspace*)items->Items[OKBottomDlg->ComboBox->ItemIndex];
      return CResult(ERR_NOERROR);
    }
  }
  return CResult(ERR_READITEMS);
}

CResult CWorkspace::sync() {
  TADOQuery *query = new TADOQuery(0);
  query->ConnectionString = udl;
  CResult r;

  if (query->Active)
    query->Close();

  if (newItems->Count > 0) {
    r = saveItemsToDB(newItems, query);
    if (r.noerror())
      newItems->Clear();
  }

  if (query->Active)
    query->Close();

  r = loadItemsFromDB(items, query);

  delete query;
  return r;
}

long CWorkspace::getItemsCount() {
  return items->Count;
}
