/*
 * CTest.cpp
 *
 *  Created on: 25 ����� 2014 �.
 *      Author: D
 */

#include <vcl.h>

#include "CTest.h"
#include "ActiveSprFormImpl.h"

CTest::CTest(AnsiString _udl) {
  udl = _udl;
  items = new TList();
  newItems = new TList();
}

CResult CTest::saveItemsToDB(TList *_items, TADOQuery *query) {
  if (_items->Count > 0) {
    query->SQL->Text =
	"INSERT INTO `test` (`testiditem`, `testItemNumber`, `testidworkspace`, `testNote`) VALUES (:testiditem, :testItemNumber, :testidworkspace, :testNote)";

    while (newItems->Count > 0) {
      item = (TTest*)_items->Items[0];
      query->Parameters->ParamByName("testiditem")->Value =
	  (__int64)item->testiditem;
      query->Parameters->ParamByName("testItemNumber")->Value =
	  item->testItemNumber;
      query->Parameters->ParamByName("testidworkspace")->Value =
	  (__int64)item->testidworkspace;
      query->Parameters->ParamByName("testNote")->Value = item->testNote;
      if (query->ExecSQL() == 1)
	_items->Delete(0);
      else
	break;
    }
  }
  return CResult(ERR_NOERROR);
}

CResult CTest::loadItemsFromDB(TList *_items, TADOQuery *query) {
  query->SQL->Text = "select * from test";
  query->Open();
  if (query->RecordCount > 0) {
    _items->Clear();
    query->First();
    while (!query->Eof) {
      item = new TTest;
      item->idtest = query->Fields->FieldByName("idtest")->AsFloat;
      item->testiditem = query->Fields->FieldByName("testiditem")->AsFloat;
      item->testItemNumber = query->Fields->FieldByName("testItemNumber")
	  ->AsString;
      item->testidworkspace = query->Fields->FieldByName("testidworkspace")
	  ->AsFloat;
      item->testNote = query->Fields->FieldByName("testNote")->AsString;

      _items->Add(item);
      query->Next();
    }
    query->Close();
    return CResult(ERR_NOERROR);
  }
  else
    return CResult(ERR_NOERROR);
}

AnsiString CTest::getItemCaption(int i) {
  item = (TTest*)items->Items[i];

  return "�" + item->testItemNumber + " " + item->testNote;
}

long CTest::getItemId(int i) {
  item = (TTest*)items->Items[i];

  return item->idtest;
}

CResult CTest::addNewItem(TTest *_item) {
  item = new TTest;
  *item = *_item;
  newItems->Add(item);
  return CResult(ERR_NOERROR);
}

CResult CTest::getItemByIndex(uint64_t i, TTest *item) {
  if (i < items->Count) {
    *item = *(TTest*)items->Items[i];
    return CResult(ERR_NOERROR);
  }
  return CResult(ERR_READITEMS);
}

CResult CTest::getItemById(uint64_t id, TTest *item) {
  if (items->Count > 0) {
    for (uint64_t i = 0; i < items->Count; i++)
      if (getItemId(i) == id) {
	*item = *(TTest*)items->Items[i];
	return CResult(ERR_NOERROR);
      }
  }
  return CResult(ERR_READITEMS);
}

CResult CTest::getItemByGui(TTest *item) {
  TActiveSprForm *OKBottomDlg;

  if (items->Count > 0) {
    OKBottomDlg = new TActiveSprForm(NULL);
    OKBottomDlg->ComboBox->Items->Clear();
    OKBottomDlg->Caption = "�����";

    for (uint64_t i = 0; i < items->Count; i++)
      OKBottomDlg->ComboBox->Items->Add(getItemCaption(i));

    OKBottomDlg->ComboBox->ItemIndex = 0;
    OKBottomDlg->ShowModal();

    if (OKBottomDlg->ModalResult == mrOk) {
      *item = *(TTest*)items->Items[OKBottomDlg->ComboBox->ItemIndex];
      return CResult(ERR_NOERROR);
    }
  }
  return CResult(ERR_READITEMS);
}

CResult CTest::sync() {
  TADOQuery *query = new TADOQuery(0);
  query->ConnectionString = udl;
  CResult r;

  if (query->Active)
    query->Close();

  if (newItems->Count > 0) {
    r = saveItemsToDB(newItems, query);
    if (r.noerror())
      newItems->Clear();
  }

  if (query->Active)
    query->Close();

  delete query;
  return r;
}
