// ----------------------------------------------------------------------------
#ifndef ActiveAddTestDialogImplH
#define ActiveAddTestDialogImplH
// ----------------------------------------------------------------------------

#include <ExtCtrls.hpp>
#include <Buttons.hpp>
#include <StdCtrls.hpp>
#include <Controls.hpp>
#include <Forms.hpp>
#include <Graphics.hpp>
#include <Classes.hpp>
#include <SysUtils.hpp>
#include <Windows.hpp>
#include <System.hpp>

// ----------------------------------------------------------------------------
class TActiveAddTestDialog : public TForm {
__published:
  TPanel *Panel1;
  TLabel *Label2;
  TComboBox *cbItem;
  TLabel *Label3;
  TComboBox *cbWorkPlace;
  TLabel *Label1;
  TEdit *editItemNumber;
  TEdit *editTestNote;
  TPanel *Panel2;
  TButton *Button2;
  TButton *Button1;

  void __fastcall HelpBtnClick(TObject *Sender);

private:
public:
  virtual __fastcall TActiveAddTestDialog(TComponent* AOwner);
};

// ----------------------------------------------------------------------------
extern PACKAGE TActiveAddTestDialog *ActiveAddTestDialog;
// ----------------------------------------------------------------------------
#endif
