/*
 * CResult.h
 *
 *  Created on: 05 ����� 2014 �.
 *      Author: D
 */

#ifndef CRESULT_H_
#define CRESULT_H_

#include <stdint.h>
#include <windows.h>
#include <vcl.h>


#define ERR_NOERROR             0x00000000
#define ERR_UDLFILEERROR        0x00000001
#define ERR_SYNCBASE            0x00000002
#define ERR_READITEMS           0x00000003
#define ERR_READPARAM           0x00000004
#define ERR_READSYSTEM          0x00000005
#define ERR_BASEVERSION         0x00000006
#define ERR_ADDTEST             0x00000007
#define ERR_ADDPARAM            0x00000008
#define ERR_READPROCBINARY      0x00000009

static char errtxt[10][255]={ "�������",
                       "������������ ���� udl ��� ������ ����������",
                       "������ ������������� � ��������",
                       "������ ������������� �������",
                       "������ ������������� ����������",
                       "������ ������������� ��������� ����������",
                       "������ - ������ ������� � ������� ����������",
                       "������ ���������� ���������",
                       "������ ���������� ���������",
					   "������ ������ �������� ������"};


class CResult {
        uint16_t result;
public:
	CResult(uint16_t code=ERR_NOERROR)
        {
                result = code;
        };
        uint16_t getResultCode()
        {
                return result;
        };
        AnsiString getResultText()
        {
                return AnsiString(errtxt[result]);
        };
	bool noerror()
        {
                return result==ERR_NOERROR;
        };
        void showMessage()
        {
                ShowMessage(getResultText());
        }
        virtual ~CResult();

        
};

#endif /* CRESULT_H_ */
