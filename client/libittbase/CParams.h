/*
 * CParams.h
 *
 *  Created on: 10 ����� 2014 �.
 *      Author: D
 */

#ifndef CPARAMS_H_
#define CPARAMS_H_

#include <system.hpp>
#include <ADODB.hpp>

#include "CResult.h" 

typedef struct
{
        uint64_t        idparam;
        AnsiString      paramName;
        AnsiString      paramNote;
} TPar;


class CParams {
        AnsiString udl;
        TList *params;
        TList *newParams;
        TPar *param;
public:
	CParams(AnsiString _udl);
	virtual ~CParams();

        CResult sync();
        CResult getParamByIndex(uint64_t i, TPar *_param);
        CResult getParamById(uint64_t id, TPar *_param);
        CResult getParamByName(AnsiString name, TPar *_param);

        CResult getParamByGui(TPar *_param);
        CResult addNewParam(TPar *_param);

        CResult saveParamsToDB(TList *_params, TADOQuery *query);
        CResult loadParamsFromDB(TList *_params, TADOQuery *query);
        AnsiString getParamCaption(int i);
        long getParamId(int i);
};

#endif /* CParamS_H_ */
