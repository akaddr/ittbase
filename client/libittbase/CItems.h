/*
 * CParams.h
 *
 *  Created on: 10 ����� 2014 �.
 *      Author: D
 */

#ifndef CITEMS_H_
#define CITEMS_H_

#include <vcl.h>
#include <ADODB.hpp>
                       
#include "CResult.h" 

typedef struct
{
        uint64_t        iditem;
        AnsiString      itemName;
        AnsiString      itemNameDec;
} TItem;

class CItems
{
        AnsiString udl;
        TList *items;
        TList *newItems;
        TItem *item;

public:
        CItems(AnsiString _udl);
        CResult getItemByIndex(uint64_t i, TItem *_item);
        CResult getItemById(uint64_t id, TItem *_item);
        CResult getItemByGui(TItem *_item);
        CResult saveItemsToDB(TList *_items, TADOQuery *query);
        CResult loadItemsFromDB(TList *_items, TADOQuery *query);
        AnsiString getItemCaption(int i);
        long getItemId(int i);
        CResult addNewItem(TItem *_item);
        CResult sync();
        long getItemsCount();
};

#endif /* CITEMS_H_ */
