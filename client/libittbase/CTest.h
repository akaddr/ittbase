/*
 * CTest.h
 *
 *  Created on: 10 ����� 2014 �.
 *      Author: D
 */

#ifndef CTEST_H_
#define CTEST_H_

#include <vcl.h>
#include <ADODB.hpp>

#include "CResult.h"

typedef struct
{
        uint64_t        idtest;
        uint64_t        testiditem;
        AnsiString      testItemNumber;
        uint64_t        testidworkspace;
        AnsiString      testNote;
} TTest;


class CTest
{
        AnsiString udl;
        TList *items;
        TList *newItems;
        TTest *item;

public:
        CTest(AnsiString _udl);
        CResult getItemByIndex(uint64_t i, TTest *_item);
        CResult getItemById(uint64_t id, TTest *_item);
        CResult getItemByGui(TTest *_item);
        CResult saveItemsToDB(TList *_items, TADOQuery *query);
        CResult loadItemsFromDB(TList *_items, TADOQuery *query);
        AnsiString getItemCaption(int i);
        long getItemId(int i);
        CResult addNewItem(TTest *_item);
        CResult sync();
};

#endif /* CTEST_H_ */
