/*
 * CItems.cpp
 *
 *  Created on: 10 ����� 2014 �.
 *      Author: D
 */

#include <vcl.h>

#include "CItems.h"
#include "ActiveSprFormImpl.h"

CItems::CItems(AnsiString _udl) {
  udl = _udl;
  items = new TList();
  newItems = new TList();
}

CResult CItems::saveItemsToDB(TList *_items, TADOQuery *query) {
  if (_items->Count > 0) {
    query->SQL->Text =
	"INSERT INTO `item` (`itemName`, `itemNameDec`) VALUES (:itemName, :itemNameDec)";
    while (newItems->Count > 0) {
      item = (TItem*)_items->Items[0];
      query->Parameters->ParamByName("itemName")->Value = item->itemName;
      query->Parameters->ParamByName("itemNameDec")->Value = item->itemNameDec;
      if (query->ExecSQL() == 1)
	_items->Delete(0);
      else
	break;
    }
  }
  return CResult(ERR_NOERROR);
}

CResult CItems::loadItemsFromDB(TList *_items, TADOQuery *query) {
  query->SQL->Text = "select * from item";
  query->Open();
  if (query->RecordCount > 0) {
    _items->Clear();
    query->First();
    while (!query->Eof) {
      item = new TItem;
      item->iditem = query->Fields->FieldByName("iditem")->AsFloat;
      item->itemName = query->Fields->FieldByName("itemName")->AsString;
      item->itemNameDec = query->Fields->FieldByName("itemNameDec")->AsString;

      _items->Add(item);
      query->Next();
    }
    query->Close();
    return CResult(ERR_NOERROR);
  }
  else
    return CResult(ERR_NOERROR);
}

AnsiString CItems::getItemCaption(int i) {
  item = (TItem*)items->Items[i];

  return item->itemName + " " + item->itemNameDec;
}

long CItems::getItemId(int i) {
  item = (TItem*)items->Items[i];

  return item->iditem;
}

CResult CItems::addNewItem(TItem *_item) {
  item = new TItem;
  *item = *_item;
  newItems->Add(item);
  return CResult(ERR_NOERROR);
}

CResult CItems::getItemByIndex(uint64_t i, TItem *item) {
  if (i < items->Count) {
    *item = *(TItem*)items->Items[i];
    return CResult(ERR_NOERROR);
  }
  return CResult(ERR_READITEMS);
}

CResult CItems::getItemById(uint64_t id, TItem *item) {
  if (items->Count > 0) {
    for (uint64_t i = 0; i < items->Count; i++)
      if (getItemId(i) == id) {
	*item = *(TItem*)items->Items[i];
	return CResult(ERR_NOERROR);
      }
  }
  return CResult(ERR_READITEMS);
}

CResult CItems::getItemByGui(TItem *item) {
  TActiveSprForm *OKBottomDlg;

  if (items->Count > 0) {
    OKBottomDlg = new TActiveSprForm(NULL);
    OKBottomDlg->ComboBox->Items->Clear();
    OKBottomDlg->Caption = "�����";

    for (uint64_t i = 0; i < items->Count; i++)
      OKBottomDlg->ComboBox->Items->Add(getItemCaption(i));

    OKBottomDlg->ComboBox->ItemIndex = 0;
    OKBottomDlg->ShowModal();

    if (OKBottomDlg->ModalResult == mrOk) {
      *item = *(TItem*)items->Items[OKBottomDlg->ComboBox->ItemIndex];
      return CResult(ERR_NOERROR);
    }
  }
  return CResult(ERR_READITEMS);
}

CResult CItems::sync() {
  TADOQuery *query = new TADOQuery(0);
  query->ConnectionString = udl;
  CResult r;

  if (query->Active)
    query->Close();

  if (newItems->Count > 0) {
    r = saveItemsToDB(newItems, query);
    if (r.noerror())
      newItems->Clear();
  }

  if (query->Active)
    query->Close();

  r = loadItemsFromDB(items, query);

  delete query;
  return r;
}

long CItems::getItemsCount() {
  return items->Count;
}
