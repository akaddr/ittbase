//---------------------------------------------------------------------------

#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ADODB.hpp>
#include <DB.hpp>

#include "CITTBase.h"

#include <DBGrids.hpp>
#include <Grids.hpp>
#include <Menus.hpp>
#include <Dialogs.hpp>
//---------------------------------------------------------------------------
class TformMain : public TForm
{
__published:	// IDE-managed Components
        TButton *Button1;
        TButton *Button2;
        TButton *Button3;
        TEdit *editItemName;
        TLabel *Label1;
        TEdit *editItemNameDec;
        TLabel *Label2;
        TGroupBox *GroupBox1;
        TLabel *Label3;
        TLabel *Label4;
        TButton *Button4;
        TButton *Button5;
        TEdit *editParamName;
        TEdit *editParamNote;
        TGroupBox *GroupBox2;
        TButton *Button6;
        TGroupBox *GroupBox3;
        TButton *Button7;
        TGroupBox *GroupBox4;
        TButton *Button8;
        TEdit *editWorkspaceName;
        TEdit *editWorkspaceNumber;
        TEdit *editWorkspaceMisc;
        TButton *Button9;
        TGroupBox *GroupBox5;
        TButton *Button10;
        TEdit *editNumber;
        TEdit *editNote;
        TButton *Button11;
        TGroupBox *GroupBox6;
        TButton *Button12;
        TButton *Button13;
        TButton *Button14;
        TButton *Button15;
        TButton *Button16;
        TLabel *Label5;
        TLabel *Label6;
        TLabel *Label7;
        TEdit *Edit1;
        TEdit *Edit2;
        TButton *Button17;
        TButton *Button18;
        TButton *Button19;
        TOpenDialog *OpenDialog1;
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall Button3Click(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall Button4Click(TObject *Sender);
        void __fastcall Button5Click(TObject *Sender);
        void __fastcall Button6Click(TObject *Sender);
        void __fastcall Button7Click(TObject *Sender);
        void __fastcall Button8Click(TObject *Sender);
        void __fastcall Button9Click(TObject *Sender);
        void __fastcall editWorkspaceNameClick(TObject *Sender);
        void __fastcall Button10Click(TObject *Sender);
        void __fastcall Button11Click(TObject *Sender);
        void __fastcall Button12Click(TObject *Sender);
        void __fastcall Button13Click(TObject *Sender);
        void __fastcall Button14Click(TObject *Sender);
        void __fastcall Button15Click(TObject *Sender);
        void __fastcall Button17Click(TObject *Sender);
        void __fastcall Button18Click(TObject *Sender);
        void __fastcall Button16Click(TObject *Sender);
        void __fastcall Button19Click(TObject *Sender);
private:              
        CITTBase *ittbase;
public:		// User declarations
        __fastcall TformMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TformMain *formMain;
//---------------------------------------------------------------------------
#endif
