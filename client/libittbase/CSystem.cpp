/*
 * CItems.cpp
 *
 *  Created on: 10 ����� 2014 �.
 *      Author: D
 */

#include <vcl.h>

#include "CSystem.h"

CSystem::CSystem(AnsiString _udl) {
  udl = _udl;
}

CResult CSystem::sync() {
  TADOQuery *query = new TADOQuery(0);
  query->ConnectionString = udl;
  CResult r;

  if (query->Active)
    query->Close();

  query->SQL->Text = "select * from system";
  query->Open();
  if (query->RecordCount > 0) {
    system.version = query->Fields->FieldByName("version")->AsFloat;
    return CResult(ERR_NOERROR);
  }
  else
    return CResult(ERR_READSYSTEM);
}

uint64_t CSystem::getVersion() {
  return system.version;
}
