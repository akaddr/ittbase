// ---------------------------------------------------------------------------
// ITTBaseImpl1.h : Declaration of the TITTBaseImpl
// ---------------------------------------------------------------------------
#ifndef ITTBaseImplH
#define ITTBaseImplH

#include <ComServ.hpp>
#include <axbase.h>
#include "ActiveITTBase_TLB.h"

#include "CITTBase.h"

// ---------------------------------------------------------------------------
// TITTBaseImpl     Implements IITTBase, default interface of ITTBase
// ThreadingModel : tmApartment
// Dual Interface : TRUE
// Event Support  : FALSE
// Description    :
// ---------------------------------------------------------------------------
class DAX_COM_CLASS TITTBaseImpl : public TCppAutoObject<IITTBase> {
  typedef _COM_CLASS inherited;

public:
  __fastcall TITTBaseImpl();
  __fastcall TITTBaseImpl(const System::_di_IInterface Controller);
  __fastcall TITTBaseImpl(Comobj::TComObjectFactory* Factory,
      const System::_di_IInterface Controller);

  // IITTBase
protected:
  CITTBase *ittbase;

  STDMETHOD(addProcessValue(long idparam, double value, BSTR note));
  STDMETHOD(addProcessValueWithFile(long idparam, double value, BSTR note, BSTR pathBinary,
          BSTR pathMime));
  STDMETHOD(getDBString(BSTR* udlFilePath));
  STDMETHOD(getItemCaption(BSTR* caption));
  STDMETHOD(getLastId(long* id));
  STDMETHOD(getLastResult(__int64* code, BSTR* text));
  STDMETHOD(getProcBinaryByIdToFile(long idproc, BSTR file));
  STDMETHOD(getTestCaption(BSTR* caption));
  STDMETHOD(getWorkspaceCaption(BSTR* caption));
  STDMETHOD(newTest(BSTR itemNumber, BSTR testNote));
  STDMETHOD(newTestByGui());
  STDMETHOD(resetConnection());
  STDMETHOD(selectItemByGui());
  STDMETHOD(selectItemById(long id));
  STDMETHOD(selectTestByGui());
  STDMETHOD(selectTestById(long id));
  STDMETHOD(selectWorkSpaceByGui());
  STDMETHOD(selectWorkspaceById(long id));
  STDMETHOD(sync());

};

#endif //ITTBaseImpl1H
