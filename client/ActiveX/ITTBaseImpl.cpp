// ---------------------------------------------------------------------------
// ITTBASEIMPL1 : Implementation of TITTBaseImpl (CoClass: ITTBase, Interface: IITTBase)
// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "ITTBaseImpl.h"

// ---------------------------------------------------------------------------
// TITTBaseImpl
// ---------------------------------------------------------------------------
__fastcall TITTBaseImpl::TITTBaseImpl() {
  ittbase = new CITTBase();
}

// ---------------------------------------------------------------------------
// TITTBaseImpl
// ---------------------------------------------------------------------------
__fastcall TITTBaseImpl::TITTBaseImpl(const System::_di_IInterface Controller)
    : inherited(Controller) {
  ittbase = new CITTBase();

}

// ---------------------------------------------------------------------------
// TITTBaseImpl
// ---------------------------------------------------------------------------
__fastcall TITTBaseImpl::TITTBaseImpl(Comobj::TComObjectFactory* Factory,
    const System::_di_IInterface Controller) : inherited(Factory, Controller) {
  ittbase = new CITTBase();

}

// ---------------------------------------------------------------------------
// TITTBaseImpl - Class Factory
// ---------------------------------------------------------------------------
static void createFactory() {
  new TCppAutoObjectFactory<TITTBaseImpl>(Comserv::GetComServer(),
      __classid(TITTBaseImpl), CLSID_ITTBase, Comobj::ciMultiInstance,
      Comobj::tmApartment);
}
#pragma startup createFactory 32

STDMETHODIMP TITTBaseImpl::addProcessValue(long idparam, double value, BSTR note)


{
  try {
    ittbase->addProcessValue(idparam, value, note, "", "");

  }
  catch (Exception &e) {
    return Error(e.Message.c_str(), IID_IITTBase);
  }
  return S_OK;

}

STDMETHODIMP TITTBaseImpl::addProcessValueWithFile(long idparam, double value, BSTR note,
          BSTR pathBinary, BSTR pathMime) {
  try {
    ittbase->addProcessValue(idparam, value, note, pathBinary, pathMime);

  }
  catch (Exception &e) {
    return Error(e.Message.c_str(), IID_IITTBase);
  }
  return S_OK;

}

STDMETHODIMP TITTBaseImpl::getDBString(BSTR* udlFilePath) {
  try {
    WideString s = ittbase->getDBString();
    *udlFilePath = s.Detach();

  }
  catch (Exception &e) {
    return Error(e.Message.c_str(), IID_IITTBase);
  }
  return S_OK;

}

STDMETHODIMP TITTBaseImpl::getItemCaption(BSTR* caption) {
  try {
    WideString s = ittbase->getItemCaption();
    *caption = s.Detach();
  }
  catch (Exception &e) {
    return Error(e.Message.c_str(), IID_IITTBase);
  }
  return S_OK;

}

STDMETHODIMP TITTBaseImpl::getLastId(long* id) {
  try {
    *id = ittbase->getLastId();

  }
  catch (Exception &e) {
    return Error(e.Message.c_str(), IID_IITTBase);
  }
  return S_OK;

}

STDMETHODIMP TITTBaseImpl::getLastResult(__int64* code, BSTR* text) {
  try {
    WideString s = ittbase->getLastResult(code);
    *text = s.Detach();

  }
  catch (Exception &e) {
    return Error(e.Message.c_str(), IID_IITTBase);
  }
  return S_OK;

}

STDMETHODIMP TITTBaseImpl::getProcBinaryByIdToFile(long idproc, BSTR file) {
  try {
    ittbase->getProcBinaryByIdToFile(idproc, file);

  }
  catch (Exception &e) {
    return Error(e.Message.c_str(), IID_IITTBase);
  }
  return S_OK;

}

STDMETHODIMP TITTBaseImpl::getTestCaption(BSTR* caption) {
  try {
    WideString s = ittbase->getTestCaption();
    *caption = s.Detach();

  }
  catch (Exception &e) {
    return Error(e.Message.c_str(), IID_IITTBase);
  }
  return S_OK;

}

STDMETHODIMP TITTBaseImpl::getWorkspaceCaption(BSTR* caption) {
  try {
    WideString s = ittbase->getWorkspaceCaption();
    *caption = s.Detach();

  }
  catch (Exception &e) {
    return Error(e.Message.c_str(), IID_IITTBase);
  }
  return S_OK;

}

STDMETHODIMP TITTBaseImpl::newTest(BSTR itemNumber, BSTR testNote) {
  try {
    ittbase->newTest(itemNumber, testNote);

  }
  catch (Exception &e) {
    return Error(e.Message.c_str(), IID_IITTBase);
  }
  return S_OK;

}

STDMETHODIMP TITTBaseImpl::newTestByGui() {
  try {
    ittbase->newTestByGui();

  }
  catch (Exception &e) {
    return Error(e.Message.c_str(), IID_IITTBase);
  }
  return S_OK;
}

STDMETHODIMP TITTBaseImpl::resetConnection() {
  try {
    ittbase->resetConnection();

  }
  catch (Exception &e) {
    return Error(e.Message.c_str(), IID_IITTBase);
  }
  return S_OK;
}

STDMETHODIMP TITTBaseImpl::selectItemByGui() {
  try {
    ittbase->selectItemByGui();

  }
  catch (Exception &e) {
    return Error(e.Message.c_str(), IID_IITTBase);
  }
  return S_OK;
}

STDMETHODIMP TITTBaseImpl::selectItemById(long id) {
  try {
    ittbase->selectItemById(id);

  }
  catch (Exception &e) {
    return Error(e.Message.c_str(), IID_IITTBase);
  }
  return S_OK;

}

STDMETHODIMP TITTBaseImpl::selectTestByGui() {
  try {
    ittbase->selectTestByGui();

  }
  catch (Exception &e) {
    return Error(e.Message.c_str(), IID_IITTBase);
  }
  return S_OK;
}

STDMETHODIMP TITTBaseImpl::selectTestById(long id) {
  try {
    ittbase->selectTestById(id);

  }
  catch (Exception &e) {
    return Error(e.Message.c_str(), IID_IITTBase);
  }
  return S_OK;

}

STDMETHODIMP TITTBaseImpl::selectWorkSpaceByGui() {
  try {
    ittbase->selectWorkspaceByGui();

  }
  catch (Exception &e) {
    return Error(e.Message.c_str(), IID_IITTBase);
  }
  return S_OK;
}

STDMETHODIMP TITTBaseImpl::selectWorkspaceById(long id) {
  try {
    ittbase->selectWorkspaceById(id);

  }
  catch (Exception &e) {
    return Error(e.Message.c_str(), IID_IITTBase);
  }
  return S_OK;

}

STDMETHODIMP TITTBaseImpl::sync() {
  try {
    ittbase->sync();

  }
  catch (Exception &e) {
    return Error(e.Message.c_str(), IID_IITTBase);
  }
  return S_OK;
}
