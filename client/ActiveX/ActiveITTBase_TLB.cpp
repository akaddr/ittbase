// ************************************************************************ //
// WARNING
// -------
// The types declared in this file were generated from data read from a
// Type Library. If this type library is explicitly or indirectly (via
// another type library referring to this type library) re-imported, or the
// 'Refresh' command of the Type Library Editor activated while editing the
// Type Library, the contents of this file will be regenerated and all
// manual modifications will be lost.
// ************************************************************************ //

// $Rev: 92848 $
// File generated on 12.03.2019 1:30:02 from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\Projects\itt_base\client\ActiveX\ActiveITTBase (1)
// LIBID: {9ABEB3D1-4748-4113-9860-4A6E596C1C0D}
// LCID: 0
// Helpfile:
// HelpString:
// DepndLst:
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //

#include <vcl.h>
#pragma hdrstop

#include "ActiveITTBase_TLB.h"

#if !defined(__PRAGMA_PACKAGE_SMART_INIT)
#define      __PRAGMA_PACKAGE_SMART_INIT
#pragma package(smart_init)
#endif

namespace Activeittbase_tlb
{


// *********************************************************************//
// GUIDS declared in the TypeLibrary
// *********************************************************************//
const GUID LIBID_ActiveITTBase = {0x9ABEB3D1, 0x4748, 0x4113,{ 0x98, 0x60, 0x4A,0x6E, 0x59, 0x6C,0x1C, 0x0D} };
const GUID IID_IITTBase = {0x98A147EA, 0xE57A, 0x4956,{ 0x94, 0x28, 0xFC,0x71, 0x18, 0xE3,0xAC, 0x11} };
const GUID CLSID_ITTBase = {0xD9FA37B7, 0x9930, 0x45F8,{ 0x9C, 0xF2, 0x2C,0xDC, 0x81, 0xB7,0x92, 0x72} };

};     // namespace Activeittbase_tlb

