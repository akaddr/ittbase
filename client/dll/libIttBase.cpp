// Important note about DLL memory management when your DLL uses the
// static version of the RunTime Library:
//
// If your DLL exports any functions that pass String objects (or structs/
// classes containing nested Strings) as parameter or function results,
// you will need to add the library MEMMGR.LIB to both the DLL project and
// any other projects that use the DLL.  You will also need to use MEMMGR.LIB
// if any other projects which use the DLL will be performing new or delete
// operations on any non-TObject-derived classes which are exported from the
// DLL. Adding MEMMGR.LIB to your project will change the DLL and its calling
// EXE's to use the BORLNDMM.DLL as their memory manager.  In these cases,
// the file BORLNDMM.DLL should be deployed along with your DLL.
//
// To avoid using BORLNDMM.DLL, pass string information using "char *" or
// ShortString parameters.
//
// If your DLL uses the dynamic version of the RTL, you do not need to
// explicitly add MEMMGR.LIB as this will be done implicitly for you

#include <vcl.h>
#include <windows.h>
#include <system.hpp>

#include <CITTBase.h>

USEFORM("ActiveAddTestDialogImpl.cpp", ActiveAddTestDialog);
USEFORM("ActiveSprFormImpl.cpp", ActiveSprForm);

#pragma hdrstop
#pragma argsused

CITTBase *base;

int WINAPI DllEntryPoint(HINSTANCE hinst, unsigned long reason,
	void* lpReserved) {

	if (base == NULL) {
		base = new CITTBase();
	}

	return 1;
}

extern "C" __declspec(dllexport) int resetConnection() {
	return base->resetConnection().getResultCode();
}

extern "C" __declspec(dllexport) int sync() {

	return base->sync().getResultCode();
}

extern "C" __declspec(dllexport) int newTest(char testItemNumber[255],
	char testNote[255]) {
	return base->newTest(AnsiString(testItemNumber), AnsiString(testNote))
		.getResultCode();
}

extern "C" __declspec(dllexport) int addProcessValue(char * param_name,
	double procvalue, char * procnote, char * filebinary, char * fileMime) {
	return base->addProcessValue(AnsiString(param_name), procvalue,
		AnsiString(procnote), AnsiString(filebinary), AnsiString(fileMime))
		.getResultCode();
}

extern "C" __declspec(dllexport) void MyShowMessage(char msg[255]) {
	ShowMessage(AnsiString(msg));
}
