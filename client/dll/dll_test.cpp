//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "dll_test.h"
#include "importIttBase.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
	HINSTANCE h = LoadLibrary(TEXT("ittbase.dll"));

	if (h==NULL)
		ShowMessage("Error");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	resetConnection();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
	ShowMessage(sync());
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
	newTest(AnsiString("Test dll").c_str(),AnsiString("Test dll").c_str());
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button5Click(TObject *Sender)
{
	MyShowMessage(Edit1->Text.c_str());	
}
//---------------------------------------------------------------------------

