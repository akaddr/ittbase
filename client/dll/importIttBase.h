//
//extern int __declspec(dllimport) __stdcall resetConnection();
//
//extern int __declspec(dllimport) __stdcall sync();
//
//extern int __declspec(dllimport) __stdcall newTest(char testItemNumber[255],
//	char testNote[255]);
//
//extern int __declspec(dllimport) __stdcall addProcessValue(char * param_name,
//	double procvalue, char * procnote, char * filebinary, char * fileMime);
//
//extern void __declspec(dllimport) __stdcall MyShowMessage(char msg[255]);


extern "C" __declspec(dllexport) __stdcall int resetConnection();

extern "C" __declspec(dllexport)  __stdcall int sync();

extern "C" __declspec(dllexport) __stdcall int newTest(char testItemNumber[255],
	char testNote[255]);

extern "C" __declspec(dllexport) __stdcall int addProcessValue(char * param_name,
	double procvalue, char * procnote, char * filebinary, char * fileMime);

extern "C" __declspec(dllexport) __stdcall void MyShowMessage(char msg[255]);


