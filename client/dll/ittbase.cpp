#include <vcl.h>
#include <windows.h>

#pragma hdrstop
#pragma argsused

#include "CITTBase.h"
CITTBase base;

int WINAPI DllEntryPoint(HINSTANCE hinst, unsigned long reason, void* lpReserved)
{
	return 1;
}

extern "C" __declspec(dllexport) __stdcall void resetConnection() {

	base.resetConnection();
}

extern "C" __declspec(dllexport) __stdcall void newTest(char testItemNumber[255],
	char testNote[255]) {

	if (base.sync().getResultCode()!=ERR_NOERROR)
	ShowMessage("������ �������������");

	base.newTestByGui(AnsiString(testItemNumber), AnsiString(testNote))
		.getResultCode();
}

extern "C" __declspec(dllexport) __stdcall void addProcessValue(char * param_name,
	double procvalue, char * procnote, char * filebinary, char * fileMime) {

	base.addProcessValue(AnsiString(param_name), procvalue,
		AnsiString(procnote), AnsiString(filebinary), AnsiString(fileMime))
		.getResultCode();
}

extern "C" __declspec(dllexport) __stdcall void test() {
	ShowMessage(AnsiString("Start test..."));
	CITTBase *b = new CITTBase();
	b->sync();
	b->newTestByGui("Hello", "Hello2");
	delete b;
}


