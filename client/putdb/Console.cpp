// ---------------------------------------------------------------------------

#include <System.hpp>
#include <vcl.h>
#pragma hdrstop

#include "Console.h"

#pragma package(smart_init)
// ---------------------------------------------------------------------------

// Important: Methods and properties of objects in VCL can only be
// used in a method called using Synchronize, for example:
//
// Synchronize(&UpdateCaption);
//
// where UpdateCaption could look like:
//
// void __fastcall Console::UpdateCaption()
// {
// Form1->Caption = "Updated in a thread";
// }
// ---------------------------------------------------------------------------

__fastcall Console::Console(bool CreateSuspended) : TThread(CreateSuspended) {
}

// ---------------------------------------------------------------------------
void __fastcall Console::Execute() {
	ShowMessage("Hello");
	try {
		base = new TITTBase(NULL);
		base->sync();
	}
	catch (Exception &e) {
		ShowMessage(e.ToString());
	}
	ShowMessage("�������");

	// ---- Place thread code here ----
}
// ---------------------------------------------------------------------------
