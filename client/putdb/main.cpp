//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ActiveITTBase_OCX"
#pragma resource "*.dfm"
TfmPutDb *fmPutDb;
//---------------------------------------------------------------------------
__fastcall TfmPutDb::TfmPutDb(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TfmPutDb::FormCreate(TObject *Sender)
{
	ittbase->sync();
}
//---------------------------------------------------------------------------
void __fastcall TfmPutDb::Button1Click(TObject *Sender)
{
	BSTR s;
	__int64 c;

	ittbase->newTestByGui();
	ittbase->getLastResult(&c,&s);

	if (c==0) {
		ittbase->getItemCaption(&s);
		Label5->Caption = s;

		ittbase->getTestCaption(&s);
		Label6->Caption = s;

		ittbase->getWorkspaceCaption(&s);
		Label7->Caption = s;
	}
}
//---------------------------------------------------------------------------
void __fastcall TfmPutDb::Button2Click(TObject *Sender)
{
	int paramIdx = cbParam->ItemIndex;
	double value = lbeValue->Text.ToDouble();
	WideString note = lbeNote->Text;
	WideString fileName = lbeFileName->Text;
	WideString fileNameBin="";
		BSTR s;
	__int64 c;

	ittbase->addProcessValueWithFile(paramIdx,value,note.c_bstr(),fileNameBin.c_bstr(),fileName.c_bstr());
	ittbase->getLastResult(&c,&s);
	if (c==0) {
            ShowMessage("�������");
	}

}
//---------------------------------------------------------------------------

void __fastcall TfmPutDb::Button3Click(TObject *Sender)
{
	if (FileOpenDialog1->Execute()) {
		  lbeFileName->Text = FileOpenDialog1->FileName;
	}
}
//---------------------------------------------------------------------------

void __fastcall TfmPutDb::Button4Click(TObject *Sender)
{
	ittbase->resetConnection();
}
//---------------------------------------------------------------------------

