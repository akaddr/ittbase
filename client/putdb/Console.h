//---------------------------------------------------------------------------

#ifndef ConsoleH
#define ConsoleH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>

#include "ActiveITTBase_OCX.h"

//---------------------------------------------------------------------------
class Console : public TThread
{
private:
	TITTBase *base;

protected:
	void __fastcall Execute();
public:
	__fastcall Console(bool CreateSuspended);
};
//---------------------------------------------------------------------------
#endif
