object fmPutDb: TfmPutDb
  Left = 0
  Top = 0
  Caption = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1079#1072#1087#1080#1089#1080' '#1074' '#1073#1072#1079#1091
  ClientHeight = 374
  ClientWidth = 817
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = [fsBold]
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 16
  object GroupBox2: TGroupBox
    Left = 16
    Top = 8
    Width = 380
    Height = 305
    Caption = '1. '#1044#1086#1073#1072#1074#1080#1090#1100' '#1076#1077#1090#1072#1083#1100'/'#1091#1079#1077#1083'/'#1080#1079#1076#1077#1083#1080#1077
    TabOrder = 1
    object Label1: TLabel
      Left = 24
      Top = 41
      Width = 56
      Height = 16
      Caption = #1048#1079#1076#1077#1083#1080#1077
    end
    object Label2: TLabel
      Left = 24
      Top = 81
      Width = 43
      Height = 16
      Caption = #1053#1086#1084#1077#1088
    end
    object Label3: TLabel
      Left = 24
      Top = 121
      Width = 100
      Height = 16
      Caption = #1056#1072#1073#1086#1095#1077#1077' '#1084#1077#1089#1090#1086
    end
    object Label5: TLabel
      Left = 168
      Top = 41
      Width = 116
      Height = 16
      Caption = '                             '
    end
    object Label6: TLabel
      Left = 168
      Top = 81
      Width = 116
      Height = 16
      Caption = '                             '
    end
    object Label7: TLabel
      Left = 168
      Top = 121
      Width = 116
      Height = 16
      Caption = '                             '
    end
    object Button1: TButton
      Left = 24
      Top = 258
      Width = 321
      Height = 33
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      TabOrder = 0
      OnClick = Button1Click
    end
  end
  object GroupBox1: TGroupBox
    Left = 415
    Top = 8
    Width = 380
    Height = 305
    Caption = '2. '#1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1072#1088#1072#1084#1077#1090#1088
    TabOrder = 0
    object cbParam: TComboBox
      Left = 16
      Top = 32
      Width = 345
      Height = 24
      ItemIndex = 0
      TabOrder = 0
      Text = #1047#1072#1093#1074#1072#1090', '#1043#1094
      Items.Strings = (
        #1047#1072#1093#1074#1072#1090', '#1043#1094
        #1048#1085#1090#1077#1075#1088#1072#1083#1100#1085#1086#1077' '#1088#1072#1089#1089#1077#1080#1074#1072#1085#1080#1077', ppm'
        #1055#1088#1086#1087#1091#1089#1082#1072#1085#1080#1077', ppm'
        #1055#1088#1086#1090#1103#1078#1077#1085#1085#1086#1089#1090#1100' '#1076#1077#1092#1077#1082#1090#1086#1074', '#1084#1082#1084'/'#1084#1084'^2'
        #1055#1086#1075#1083#1086#1097#1077#1085#1080#1077', ppm'
        #1055#1086#1090#1077#1088#1080' '#1074' '#1088#1077#1079#1086#1085#1072#1090#1086#1088#1077', ppm'
        #1060#1072#1079#1086#1074#1099#1081' '#1089#1076#1074#1080#1075' '#1074' '#1088#1077#1079#1086#1085#1072#1090#1086#1088#1077', '#1075#1088#1072#1076
        #1052#1072#1089#1096#1090#1072#1073#1085#1099#1081' '#1082#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090
        #1057#1084#1077#1097#1077#1085#1080#1077' '#1085#1091#1083#1103
        #1057#1083#1091#1095#1072#1081#1085#1072#1103' '#1089#1086#1089#1090#1072#1074#1083#1103#1102#1097#1072#1103' '#1074' '#1079#1072#1087#1091#1089#1082#1077
        #1057#1083#1091#1095#1072#1081#1085#1072#1103' '#1089#1086#1089#1090#1072#1074#1083#1103#1102#1097#1072#1103' '#1086#1090' '#1079#1072#1087#1091#1089#1082#1072' '#1082' '#1079#1072#1087#1091#1089#1082#1091
        #1064#1091#1084#1086#1074#1072#1103' '#1089#1086#1089#1090#1072#1074#1083#1103#1102#1097#1072#1103
        #1050#1088#1091#1090#1080#1079#1085#1072' '#1087#1100#1077#1079#1086#1082#1086#1088#1088#1077#1082#1090#1086#1088#1072', '#1042'/?'
        #1056#1072#1073#1086#1095#1080#1081' '#1090#1086#1082', '#1084#1040
        #1055#1086#1088#1086#1075#1086#1074#1099#1081' '#1090#1086#1082', '#1084#1040
        #1056#1072#1076#1080#1091#1089', '#1084#1084
        #1054#1090#1082#1083#1086#1085#1077#1085#1080#1077' '#1086#1090' '#1089#1092#1077#1088#1080#1095#1085#1086#1089#1090#1080', '#1084#1082#1084
        #1064#1077#1088#1086#1093#1086#1074#1072#1090#1086#1089#1090#1100', rms, A'
        #1064#1077#1088#1086#1093#1086#1074#1072#1090#1086#1089#1090#1100', pk-pk, A'
        #1056#1072#1089#1089#1077#1080#1074#1072#1085#1080#1077' '#1085#1072' '#1076#1077#1092#1077#1082#1090#1072#1093', ppb'
        #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1076#1077#1092#1077#1082#1090#1086#1074
        #1056#1072#1079#1084#1077#1088' '#1084#1072#1082#1089' '#1076#1077#1092#1077#1082#1090#1072', '#1084#1082#1084
        #1056#1072#1079#1084#1077#1088' '#1076#1080#1072#1092#1088#1072#1075#1084#1099' '#1087#1086' '#1086#1089#1080' '#1061', '#1084#1084
        #1056#1072#1079#1084#1077#1088' '#1076#1080#1072#1092#1088#1072#1075#1084#1099' '#1087#1086' '#1086#1089#1080' '#1059', '#1084#1084)
    end
    object lbeValue: TLabeledEdit
      Left = 16
      Top = 88
      Width = 345
      Height = 24
      EditLabel.Width = 63
      EditLabel.Height = 16
      EditLabel.Caption = #1047#1085#1072#1095#1077#1085#1080#1077
      TabOrder = 1
      Text = '0'
    end
    object Button2: TButton
      Left = 16
      Top = 258
      Width = 345
      Height = 33
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      TabOrder = 2
      OnClick = Button2Click
    end
    object lbeNote: TLabeledEdit
      Left = 17
      Top = 141
      Width = 344
      Height = 24
      EditLabel.Width = 82
      EditLabel.Height = 16
      EditLabel.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
      TabOrder = 3
    end
    object lbeFileName: TLabeledEdit
      Left = 16
      Top = 199
      Width = 266
      Height = 24
      EditLabel.Width = 35
      EditLabel.Height = 16
      EditLabel.Caption = #1060#1072#1081#1083
      TabOrder = 4
    end
    object Button3: TButton
      Left = 288
      Top = 199
      Width = 75
      Height = 24
      Caption = #1054#1090#1082#1088#1099#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = Button3Click
    end
  end
  object Button4: TButton
    Left = 724
    Top = 332
    Width = 75
    Height = 25
    Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    OnClick = Button4Click
  end
  object FileOpenDialog1: TFileOpenDialog
    FavoriteLinks = <>
    FileTypes = <>
    Options = []
    Left = 88
    Top = 224
  end
  object ittbase: TITTBase
    AutoConnect = False
    ConnectKind = ckRunningOrNew
    Left = 208
    Top = 328
  end
end
