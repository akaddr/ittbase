//---------------------------------------------------------------------------

#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "ActiveITTBase_OCX.h"
#include <Vcl.OleServer.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.ValEdit.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Dialogs.hpp>
//---------------------------------------------------------------------------
class TfmPutDb : public TForm
{
__published:	// IDE-managed Components
	TGroupBox *GroupBox1;
	TComboBox *cbParam;
	TLabeledEdit *lbeValue;
	TButton *Button2;
	TLabeledEdit *lbeNote;
	TLabeledEdit *lbeFileName;
	TButton *Button3;
	TFileOpenDialog *FileOpenDialog1;
	TGroupBox *GroupBox2;
	TButton *Button1;
	TLabel *Label1;
	TLabel *Label2;
	TLabel *Label3;
	TLabel *Label5;
	TLabel *Label6;
	TLabel *Label7;
	TButton *Button4;
	TITTBase *ittbase;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TfmPutDb(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfmPutDb *fmPutDb;
//---------------------------------------------------------------------------
#endif
