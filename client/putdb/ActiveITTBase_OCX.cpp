// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 28.08.2014 10:02:31 from Type Library described below.

// ************************************************************************  //
// Type Lib: d:\projects\2014\itt_base\client\ActiveX-14\Win32\Release\ActiveBase.dll (1)
// LIBID: {C7DEF385-0D25-4DF7-B0E1-E257FEF630A8}
// LCID: 0
// Helpfile: 
// HelpString: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// SYS_KIND: SYS_WIN32
// Errors:
//   Error creating palette bitmap of (TITTBase) : Server d:\projects\2014\itt_base\client\ActiveX-14\Win32\Release\ActiveBase.dll contains no icons
// ************************************************************************ //

#include <vcl.h>
#pragma hdrstop

#include <olectrls.hpp>
#include <oleserver.hpp>
#if defined(USING_ATL)
#include <atl\atlvcl.h>
#endif

#pragma option -w-8122
#include "ActiveITTBase_OCX.h"

#if !defined(__PRAGMA_PACKAGE_SMART_INIT)
#define      __PRAGMA_PACKAGE_SMART_INIT
#pragma package(smart_init)
#endif

namespace Activeittbase_tlb
{

IITTBasePtr& TITTBase::GetDefaultInterface()
{
  if (!m_DefaultIntf)
    Connect();
  return m_DefaultIntf;
}

_di_IUnknown __fastcall TITTBase::GetDunk()
{
  _di_IUnknown diUnk;
  if (m_DefaultIntf) {
    IUnknownPtr punk = m_DefaultIntf;
    diUnk = LPUNKNOWN(punk);
  }
  return diUnk;
}

void __fastcall TITTBase::Connect()
{
  if (!m_DefaultIntf) {
    _di_IUnknown punk = GetServer();
    m_DefaultIntf = punk;
    if (ServerData->EventIID != GUID_NULL)
      ConnectEvents(GetDunk());
  }
}

void __fastcall TITTBase::Disconnect()
{
  if (m_DefaultIntf) {

    if (ServerData->EventIID != GUID_NULL)
      DisconnectEvents(GetDunk());
    m_DefaultIntf.Reset();
  }
}

void __fastcall TITTBase::BeforeDestruction()
{
  Disconnect();
}

void __fastcall TITTBase::ConnectTo(IITTBasePtr intf)
{
  Disconnect();
  m_DefaultIntf = intf;
  if (ServerData->EventIID != GUID_NULL)
    ConnectEvents(GetDunk());
}

void __fastcall TITTBase::InitServerData()
{
  static Vcl::Oleserver::TServerData sd;
  sd.ClassID = CLSID_ITTBase;
  sd.IntfIID = __uuidof(IITTBase);
  sd.EventIID= GUID_NULL;
  ServerData = &sd;
}

void __fastcall TITTBase::sync(void)
{
  GetDefaultInterface()->sync();
}

void __fastcall TITTBase::resetConnection(void)
{
  GetDefaultInterface()->resetConnection();
}

void __fastcall TITTBase::getLastId(long* id/*[out]*/)
{
  GetDefaultInterface()->getLastId(id/*[out]*/);
}

void __fastcall TITTBase::selectItemByGui(void)
{
  GetDefaultInterface()->selectItemByGui();
}

void __fastcall TITTBase::selectWorkSpaceByGui(void)
{
  GetDefaultInterface()->selectWorkSpaceByGui();
}

void __fastcall TITTBase::selectTestByGui(void)
{
  GetDefaultInterface()->selectTestByGui();
}

void __fastcall TITTBase::newTestByGui(void)
{
  GetDefaultInterface()->newTestByGui();
}

void __fastcall TITTBase::newTest(BSTR itemNumber/*[in]*/, BSTR testNote/*[in]*/)
{
  GetDefaultInterface()->newTest(itemNumber/*[in]*/, testNote/*[in]*/);
}

void __fastcall TITTBase::getItemCaption(BSTR* caption/*[out]*/)
{
  GetDefaultInterface()->getItemCaption(caption/*[out]*/);
}

void __fastcall TITTBase::selectItemById(long id/*[in]*/)
{
  GetDefaultInterface()->selectItemById(id/*[in]*/);
}

void __fastcall TITTBase::selectWorkspaceById(long id/*[in]*/)
{
  GetDefaultInterface()->selectWorkspaceById(id/*[in]*/);
}

void __fastcall TITTBase::getWorkspaceCaption(BSTR* caption/*[out]*/)
{
  GetDefaultInterface()->getWorkspaceCaption(caption/*[out]*/);
}

void __fastcall TITTBase::selectTestById(long id/*[in]*/)
{
  GetDefaultInterface()->selectTestById(id/*[in]*/);
}

void __fastcall TITTBase::getTestCaption(BSTR* caption/*[out]*/)
{
  GetDefaultInterface()->getTestCaption(caption/*[out]*/);
}

void __fastcall TITTBase::addProcessValue(long idparam/*[in]*/, double value/*[in]*/, 
                                          BSTR note/*[in]*/)
{
  GetDefaultInterface()->addProcessValue(idparam/*[in]*/, value/*[in]*/, note/*[in]*/);
}

void __fastcall TITTBase::getDBString(BSTR* udlFilePath/*[out]*/)
{
  GetDefaultInterface()->getDBString(udlFilePath/*[out]*/);
}

void __fastcall TITTBase::getProcBinaryByIdToFile(long idproc/*[in]*/, BSTR file/*[in]*/)
{
  GetDefaultInterface()->getProcBinaryByIdToFile(idproc/*[in]*/, file/*[in]*/);
}

void __fastcall TITTBase::addProcessValueWithFile(long idparam/*[in]*/, double value/*[in]*/, 
                                                  BSTR note/*[in]*/, BSTR pathBinary/*[in]*/, 
                                                  BSTR pathMime/*[in]*/)
{
  GetDefaultInterface()->addProcessValueWithFile(idparam/*[in]*/, value/*[in]*/, note/*[in]*/, 
                                                 pathBinary/*[in]*/, pathMime/*[in]*/);
}

void __fastcall TITTBase::getLastResult(__int64* code/*[out]*/, BSTR* text/*[out]*/)
{
  GetDefaultInterface()->getLastResult(code/*[out]*/, text/*[out]*/);
}


};     // namespace Activeittbase_tlb


// *********************************************************************//
// The Register function is invoked by the IDE when this module is 
// installed in a Package. It provides the list of Components (including
// OCXes) implemented by this module. The following implementation
// informs the IDE of the OCX proxy classes implemented here.
// *********************************************************************//
namespace Activeittbase_ocx
{

void __fastcall PACKAGE Register()
{
  // [1]
  System::Classes::TComponentClass cls_svr[] = {
                              __classid(Activeittbase_tlb::TITTBase)
                           };
  System::Classes::RegisterComponents("ActiveX", cls_svr,
                     sizeof(cls_svr)/sizeof(cls_svr[0])-1);
}

};     // namespace Activeittbase_ocx
