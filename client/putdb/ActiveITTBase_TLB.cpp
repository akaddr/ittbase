// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 28.08.2014 10:02:31 from Type Library described below.

// ************************************************************************  //
// Type Lib: d:\projects\2014\itt_base\client\ActiveX-14\Win32\Release\ActiveBase.dll (1)
// LIBID: {C7DEF385-0D25-4DF7-B0E1-E257FEF630A8}
// LCID: 0
// Helpfile: 
// HelpString: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// SYS_KIND: SYS_WIN32
// Errors:
//   Error creating palette bitmap of (TITTBase) : Server d:\projects\2014\itt_base\client\ActiveX-14\Win32\Release\ActiveBase.dll contains no icons
// ************************************************************************ //

#include <vcl.h>
#pragma hdrstop

#include "ActiveITTBase_TLB.h"

#if !defined(__PRAGMA_PACKAGE_SMART_INIT)
#define      __PRAGMA_PACKAGE_SMART_INIT
#pragma package(smart_init)
#endif

namespace Activeittbase_tlb
{


// *********************************************************************//
// GUIDS declared in the TypeLibrary                                      
// *********************************************************************//
const GUID LIBID_ActiveITTBase = {0xC7DEF385, 0x0D25, 0x4DF7,{ 0xB0, 0xE1, 0xE2,0x57, 0xFE, 0xF6,0x30, 0xA8} };
const GUID IID_IITTBase = {0xA46701D0, 0xA6EA, 0x49F2,{ 0x9B, 0x6D, 0xA0,0xCF, 0x44, 0xE2,0xF4, 0xFD} };
const GUID CLSID_ITTBase = {0x4635F152, 0x4E10, 0x46A3,{ 0x93, 0x95, 0x33,0xC1, 0x0E, 0x93,0xA4, 0x4A} };

};     // namespace Activeittbase_tlb
