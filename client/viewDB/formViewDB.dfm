object formMain: TformMain
  Left = 0
  Top = 0
  Caption = #1055#1088#1086#1089#1084#1086#1090#1088' '#1073#1072#1079#1099' '#1076#1072#1085#1085#1099#1093
  ClientHeight = 585
  ClientWidth = 903
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    903
    585)
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 688
    Height = 122
    Anchors = [akLeft, akTop, akRight]
    Caption = #1060#1080#1083#1100#1090#1088#1099
    TabOrder = 4
    object Button3: TButton
      Left = 16
      Top = 24
      Width = 75
      Height = 25
      Caption = #1042#1089#1077
      TabOrder = 0
      OnClick = Button3Click
    end
    object Button4: TButton
      Tag = 1
      Left = 97
      Top = 24
      Width = 75
      Height = 25
      Caption = #1053#1077#1076#1077#1083#1103
      TabOrder = 1
      OnClick = Button3Click
    end
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 136
    Width = 881
    Height = 183
    Anchors = [akLeft, akTop, akRight]
    DataSource = DataSource1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnKeyDown = DBGrid1KeyDown
    Columns = <
      item
        Expanded = False
        FieldName = 'itemName'
        Title.Caption = #1048#1079#1076#1077#1083#1080#1077
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 141
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'itemNameDec'
        Title.Caption = #1044#1077#1094'. '#1085#1086#1084#1077#1088
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 146
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'testItemNumber'
        Title.Caption = #1053#1086#1084#1077#1088
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 111
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'testNote'
        Title.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 201
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'COUNT'
        Title.Caption = #1048#1079#1084#1077#1088#1077#1085#1080#1081
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 67
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'maxdate'
        Title.Caption = #1055#1086#1089#1083#1077#1076#1085#1077#1077' '#1080#1079#1084#1077#1088#1077#1085#1080#1077
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 145
        Visible = True
      end>
  end
  object DBGrid2: TDBGrid
    AlignWithMargins = True
    Left = 105
    Top = 331
    Width = 881
    Height = 246
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = DataSource2
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'procDateTime'
        Title.Caption = #1044#1072#1090#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 136
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'paramName'
        Title.Caption = #1055#1072#1088#1072#1084#1077#1090#1088
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 98
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'procvalue'
        Title.Caption = #1047#1085#1072#1095#1077#1085#1080#1077
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 137
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'procnote'
        Title.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 243
        Visible = True
      end>
  end
  object Button1: TButton
    Left = 710
    Top = 88
    Width = 178
    Height = 25
    Anchors = [akTop, akRight]
    Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1091#1089#1090#1099#1077' '#1090#1077#1089#1090#1099
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 710
    Top = 48
    Width = 178
    Height = 25
    Anchors = [akTop, akRight]
    Caption = #1057#1086#1077#1076#1080#1085#1077#1085#1080#1077' '#1089' '#1073#1072#1079#1086#1081
    TabOrder = 3
    OnClick = Button2Click
  end
  object DataSource1: TDataSource
    DataSet = ADODataSet1
    Left = 128
    Top = 72
  end
  object ADOQuery2: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from proc,param where idparam=procidparam ')
    Left = 416
    Top = 72
    object ADOQuery2idproc: TAutoIncField
      FieldName = 'idproc'
      ReadOnly = True
    end
    object ADOQuery2procidtest: TIntegerField
      FieldName = 'procidtest'
    end
    object ADOQuery2procidparam: TIntegerField
      FieldName = 'procidparam'
    end
    object ADOQuery2procDateTime: TDateTimeField
      FieldName = 'procDateTime'
    end
    object ADOQuery2procvalue: TFloatField
      FieldName = 'procvalue'
    end
    object ADOQuery2procnote: TStringField
      FieldName = 'procnote'
      Size = 255
    end
    object ADOQuery2procbinary: TBlobField
      FieldName = 'procbinary'
    end
    object ADOQuery2procMime: TBlobField
      FieldName = 'procMime'
    end
    object ADOQuery2procMimeType: TStringField
      FieldName = 'procMimeType'
      Size = 45
    end
    object ADOQuery2idparam: TAutoIncField
      FieldName = 'idparam'
      ReadOnly = True
    end
    object ADOQuery2paramName: TStringField
      FieldName = 'paramName'
      Size = 45
    end
    object ADOQuery2paramNote: TMemoField
      FieldName = 'paramNote'
      BlobType = ftMemo
    end
  end
  object DataSource2: TDataSource
    DataSet = ADOQuery2
    Left = 496
    Top = 72
  end
  object ADODataSet1: TADODataSet
    Connection = ADOConnection1
    CursorType = ctStatic
    AfterScroll = ADODataSet1AfterScroll
    CommandText = #13#10
    Parameters = <>
    Left = 216
    Top = 72
    object ADODataSet1idtest: TIntegerField
      FieldName = 'idtest'
    end
    object ADODataSet1testiditem: TIntegerField
      FieldName = 'testiditem'
    end
    object ADODataSet1testItemNumber: TStringField
      FieldName = 'testItemNumber'
      Size = 45
    end
    object ADODataSet1testidworkspace: TIntegerField
      FieldName = 'testidworkspace'
    end
    object ADODataSet1testNote: TStringField
      FieldName = 'testNote'
      Size = 255
    end
    object ADODataSet1iditem: TIntegerField
      FieldName = 'iditem'
    end
    object ADODataSet1itemName: TStringField
      FieldName = 'itemName'
      Size = 45
    end
    object ADODataSet1itemNameDec: TStringField
      FieldName = 'itemNameDec'
      Size = 45
    end
    object ADODataSet1COUNT: TLargeintField
      FieldName = 'COUNT'
      ReadOnly = True
    end
    object ADODataSet1maxdate: TDateTimeField
      FieldName = 'maxdate'
      ReadOnly = True
    end
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;Data Source=lab10' +
      ';'
    LoginPrompt = False
    Left = 312
    Top = 72
  end
end
