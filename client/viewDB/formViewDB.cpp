// ---------------------------------------------------------------------------

#include <vcl.h>
#include <System.Win.Registry.hpp>

#pragma hdrstop

#include "formViewDB.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TformMain *formMain;

// ---------------------------------------------------------------------------
__fastcall TformMain::TformMain(TComponent* Owner) : TForm(Owner) {
}
// ---------------------------------------------------------------------------


void __fastcall TformMain::DBGrid1KeyDown(TObject *Sender, WORD &Key,
	TShiftState Shift)

{
	if (Key != 46)
		return;

	TADOQuery *q = new TADOQuery(this);
	q->Connection = ADOConnection1;
	long id = ADODataSet1->FieldByName("idtest")->AsExtended;
	q->SQL->Text = "delete from test where idtest=" + IntToStr((__int64)id);
	q->ExecSQL();
	delete q;

	ADODataSet1->Close();
	ADODataSet1->Open();
}
// ---------------------------------------------------------------------------

void __fastcall TformMain::ADODataSet1AfterScroll(TDataSet *DataSet) {
	long id = DataSet->FieldByName("idtest")->AsExtended;

	if (ADOQuery2->Active)
		ADOQuery2->Close();

	ADOQuery2->SQL->Text =
		"select * from proc,param where idparam=procidparam and procidtest=" +
		IntToStr((__int64) id) + " order by idproc";

	ADOQuery2->Open();
}
// ---------------------------------------------------------------------------

void __fastcall TformMain::Button1Click(TObject *Sender) {
	TADOQuery *q = new TADOQuery(this);
	q->Connection = ADOConnection1;
	int cc = 0;

	ADODataSet1->First();
	while (!ADODataSet1->Eof) {
		long id = ADODataSet1->FieldByName("idtest")->AsExtended;
		if (q->Active)
			q->Close();
		q->SQL->Text = "SELECT COUNT(idproc) FROM proc WHERE procidtest=" +
			IntToStr((__int64)id);
		q->Open();
		if (q->FieldByName("COUNT(idproc)")->AsInteger == 0) {
			if (q->Active)
				q->Close();
			q->SQL->Text = "delete from test where idtest=" +
				IntToStr((__int64)id);
			q->ExecSQL();
			cc++;
		}
		ADODataSet1->Next();
	}

	delete q;

	ADODataSet1->Close();
	ADODataSet1->Open();
	ShowMessage("������� ������� " + IntToStr((__int64)cc));
}
// ---------------------------------------------------------------------------

void TformMain::saveDataStringToReg(AnsiString datastr) {
	TRegistry* reg = new TRegistry();
	reg->RootKey = HKEY_CURRENT_USER;
	reg->Access = KEY_WRITE;

	bool openResult = reg->OpenKey("Software\\ITT\\ActiveBase\\", true);

	if (!openResult) {
		MessageDlg("Unable to create key! Exiting.", mtError,
			TMsgDlgButtons() << mbOK, 0);
		return;
	}

	reg->WriteString("DBString", datastr);

	reg->CloseKey();
	reg->Free();
}

AnsiString TformMain::loadDataStringFromReg() {
	AnsiString result = "";

	TRegistry* reg = new TRegistry();
	reg->RootKey = HKEY_CURRENT_USER;
	reg->Access = KEY_READ;

	bool openResult = reg->OpenKey("Software\\ITT\\ActiveBase\\", true);

	if (!openResult) {
		MessageDlg("Unable to Read key! Exiting.", mtError,
			TMsgDlgButtons() << mbOK, 0);
	}
	else {
		result = reg->ReadString("DBString");
		reg->CloseKey();
	}

	reg->Free();

	return result;
}

void TformMain::resetConnection() {
	DBString = PromptDataSource(NULL, "");
	if (DBString != "")
		saveDataStringToReg(DBString);
}

void __fastcall TformMain::FormCreate(TObject *Sender) {
	DBString = loadDataStringFromReg();

	selectSQL = "SELECT *,(SELECT COUNT(*) FROM proc WHERE idtest=procidtest) AS COUNT,"
		"(SELECT MAX(procDateTime) FROM proc WHERE idtest=procidtest) AS maxdate "
		"FROM test, item ";
	whereSQL = "where testiditem=iditem ";
	orderSQL = "order by itemName, itemNameDec, testItemNumber, idtest";

	ADOConnection1->Close();
	ADODataSet1->Close();

	if (DBString == "") {
		resetConnection();
	}

	ADOConnection1->ConnectionString = DBString;
	ADODataSet1->CommandText = selectSQL+whereSQL+orderSQL;

	ADOConnection1->Open();
	ADODataSet1->Open();
}
// ---------------------------------------------------------------------------

void __fastcall TformMain::Button2Click(TObject *Sender) {
	resetConnection();
}
// ---------------------------------------------------------------------------
void __fastcall TformMain::Button3Click(TObject *Sender)
{
	TDateTime d1,d2;
	d2=Now();
	int t = ((TLabel*)Sender)->Tag;
	if (t == 0)
		whereSQL = "where testiditem=iditem ";
	if (t == 1)
	{
		d1=d2-7;
	}
}
//---------------------------------------------------------------------------

