// ---------------------------------------------------------------------------

#ifndef formViewDBH
#define formViewDBH
// ---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Data.DB.hpp>
#include <Data.Win.ADODB.hpp>
#include <Vcl.DBGrids.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.DBCGrids.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.DBCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>

// ---------------------------------------------------------------------------
class TformMain : public TForm {
__published: // IDE-managed Components

	TDBGrid *DBGrid1;
	TDataSource *DataSource1;
	TADOQuery *ADOQuery2;
	TDataSource *DataSource2;
	TDBGrid *DBGrid2;
	TADODataSet *ADODataSet1;
	TADOConnection *ADOConnection1;
	TIntegerField *ADODataSet1idtest;
	TIntegerField *ADODataSet1testiditem;
	TStringField *ADODataSet1testItemNumber;
	TIntegerField *ADODataSet1testidworkspace;
	TStringField *ADODataSet1testNote;
	TIntegerField *ADODataSet1iditem;
	TStringField *ADODataSet1itemName;
	TStringField *ADODataSet1itemNameDec;
	TButton *Button1;
	TAutoIncField *ADOQuery2idproc;
	TIntegerField *ADOQuery2procidtest;
	TIntegerField *ADOQuery2procidparam;
	TDateTimeField *ADOQuery2procDateTime;
	TFloatField *ADOQuery2procvalue;
	TStringField *ADOQuery2procnote;
	TBlobField *ADOQuery2procbinary;
	TBlobField *ADOQuery2procMime;
	TStringField *ADOQuery2procMimeType;
	TAutoIncField *ADOQuery2idparam;
	TStringField *ADOQuery2paramName;
	TMemoField *ADOQuery2paramNote;
	TButton *Button2;
	TGroupBox *GroupBox1;
	TLargeintField *ADODataSet1COUNT;
	TDateTimeField *ADODataSet1maxdate;
	TButton *Button3;
	TButton *Button4;
	void __fastcall DBGrid1KeyDown(TObject *Sender, WORD &Key,
		TShiftState Shift);
	void __fastcall ADODataSet1AfterScroll(TDataSet *DataSet);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);

private:
	AnsiString sqlDateFilter;
	AnsiString selectSQL, whereSQL, orderSQL;

public: // User declarations

	void saveDataStringToReg(AnsiString datastr);
	AnsiString loadDataStringFromReg();
	void resetConnection();

	AnsiString DBString;

	__fastcall TformMain(TComponent* Owner);
};

// ---------------------------------------------------------------------------
extern PACKAGE TformMain *formMain;
// ---------------------------------------------------------------------------
#endif
