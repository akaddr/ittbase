// ---------------------------------------------------------------------------

#pragma hdrstop
#include <stdio.h>
#include <System.hpp>

#include "version.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)

void versionPrint() {
	printf("\tVersion %s  date %s\n", VERSION, VERDATE);

}
