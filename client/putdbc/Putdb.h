/*
 * Putdb.h
 *
 *  Created on: 19 ���. 2016 �.
 *      Author: �������
 */

#ifndef PUTDB_H_
#define PUTDB_H_

#include "CITTBase.h"

#define ERR_NOERROR             0x00000000

class Putdb
{
	CITTBase *base;

public:
	Putdb();
	int exec();
	int displayHelp();
    int displayVersion();
	virtual ~Putdb();
};

#endif /* PUTDB_H_ */
