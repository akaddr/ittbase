/*
 * Putdb.cpp
 *
 *  Created on: 19 ���. 2016 �.
 *      Author: �������
 */
#include <vcl.h>
#include <stdio.h>

#include "Putdb.h"
#include "version.h"

Putdb::Putdb() {
	base = new CITTBase();
}

Putdb::~Putdb() {

}

int Putdb::exec() {
	__int64 result = 0;
	int i = 1;
	AnsiString key;

	try {
		base->sync();
		while (i < ParamCount() + 1) {
			key = ParamStr(i++);

			if (key == "-r") {
				base->resetConnection();
			}

			if (key == "-i") {
				long id = -1;
				try {
					id = ParamStr(i).ToDouble();
				}
				catch (...) {
					id = -1;
				}
				if (id > -1)
					base->selectItemById(id);
				else
					base->selectItemByGui();
			}

			if (key == "-w") {
				long id = -1;
				try {
					id = ParamStr(i).ToDouble();
				}
				catch (...) {
					id = -1;
				}
				if (id > -1)
					base->selectWorkspaceById(id);
				else
					base->selectWorkspaceByGui();

			}

			if (key == "-t") {
				WideString number = "";
				WideString desc = "";
				try {
					number = ParamStr(i);
					desc = ParamStr(i + 1);
				}
				catch (...) {
					number = "";
					desc = "";
				}
				if (number.Trim().Length() > 0)
					base->newTest(number.c_bstr(), desc.c_bstr());
				else
					base->newTestByGui();
			}

			if (key == "-p") {
				long id = -1;
				double value = 0;
				WideString note = "";
				WideString filePath = "";

				try {
					id = ParamStr(i).ToDouble();
					value = ParamStr(i + 1).ToDouble();
					note = ParamStr(i + 2);
					filePath = ParamStr(i + 3);
				}
				catch (...) {
					id = -1;
					value = 0;
					note = "";
					filePath = "";
				}

				if (id > -1) {
					if (FileExists(filePath))
						base->addProcessValue(id, value, note.c_bstr(),
						NULL, filePath.c_bstr());
					else
						base->addProcessValue(id, value, note.c_bstr(),"","");
				}

			}

		}
	}
	catch (...) {

	}
	base->getLastResult(&result);
	return result;
}

int Putdb::displayHelp() {
	versionPrint();
	printf("\n");
	printf("\t-r\t\t\t\t\tReset connection\n");
	printf("\t-i <id>\t\t\t\t\tSelect item\n");
	printf("\t-w <id>\t\t\t\t\tSelect workspace\n");
	printf("\t-t <itemNumber> <testNote>\t\tNew test\n");
	printf("\t-p <id> <value> <note> <filePath>\tAdd parametr value\n");

}

int Putdb::displayVersion() {
	versionPrint();
}
