#include <vcl.h>
#include <windows.h>

#pragma hdrstop
#pragma argsused

#include <tchar.h>
#include <stdio.h>

#include "putdb.h"

int _tmain(int argc, _TCHAR* argv[]) {
	int result = ERR_NOERROR;

	Putdb *db = new Putdb();

	if (ParamCount() > 0) {
		result = db->exec();

		if (result == 0)
			ShowMessage(L"�������.");
		else
			ShowMessage(result);
	}
	else {
		result = db->displayHelp();
	}
	return result;
}
